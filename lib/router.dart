import 'package:acnhfishingbuddy/screens/fishDetail.dart';
import 'package:acnhfishingbuddy/screens/fishDetailPageView.dart';
import 'package:acnhfishingbuddy/screens/fishList.dart';
import 'package:acnhfishingbuddy/screens/fishing.dart';
import 'package:acnhfishingbuddy/screens/home.dart';
import 'package:acnhfishingbuddy/screens/howto.dart';
import 'package:acnhfishingbuddy/screens/info.dart';
import 'package:acnhfishingbuddy/screens/myGallery.dart';
import 'package:acnhfishingbuddy/screens/purchase.dart';
import 'package:acnhfishingbuddy/screens/settings.dart';
import 'package:acnhfishingbuddy/screens/todo.dart';
import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

class FishingRouter {
  static Router router = Router();

  static Handler _mainHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => HomeScreen());

  static Handler _settingsHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => SettingsScreen());

  static Handler _fishingHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => FishingScreen());

  static Handler _fishListHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => FishListScreen());

  static Handler _fishDetailAdvancedHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
      FishDetailPageViewScreen(
          params['index'][0],
          params['list'][0])
  );

  static Handler _fishDetailHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => FishDetailScreen(params['uniqueEntryId'][0]));

  static Handler _myGalleryHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => MyGalleryScreen());

  static Handler _infoHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => InfoScreen());

  static Handler _howtoHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => HowtoScreen());

  static Handler _todoHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => TodoScreen());

  static Handler _purchaseHandler = Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => PurchaseScreen());


  static void setupRouter() {

    router.define(
      'home',
      handler: _mainHandler,
      transitionType: TransitionType.inFromRight
    );

    router.define(
        'settings',
        handler: _settingsHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'fishing',
        handler: _fishingHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'myGallery',
        handler: _myGalleryHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'info',
        handler: _infoHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'howto',
        handler: _howtoHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'todo',
        handler: _todoHandler,
        transitionType: TransitionType.inFromRight
    );

    router.define(
        'fishList',
        handler: _fishListHandler,
        transitionType: TransitionType.inFromLeft
    );

    router.define(
        'purchase',
        handler: _purchaseHandler,
        transitionType: TransitionType.inFromLeft
    );

    router.define(
      'detail/:uniqueEntryId/:index/:list',
      handler: _fishDetailAdvancedHandler,
      transitionType: TransitionType.materialFullScreenDialog
    );

    router.define(
        'detail/:uniqueEntryId',
        handler: _fishDetailHandler,
        transitionType: TransitionType.materialFullScreenDialog
    );

  }
}