import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  debugPrint('setupLocator CreatureService');
  locator.registerLazySingleton<CreatureService>(() => CreatureService());
}