import 'package:acnhfishingbuddy/locator.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';

class FishDetailScreen extends StatelessWidget {
  final String uniqueEntryId;

  FishDetailScreen(this.uniqueEntryId);

  @override
  Widget build(BuildContext context) {
    return FishScaffold(
        child: CreatureDetail(
          uniqueEntryId: uniqueEntryId,
        ),
        title: locator<CreatureService>().getFishName(uniqueEntryId));
  }
}
