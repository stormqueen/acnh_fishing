import 'dart:convert';

import 'package:acnhfishingbuddy/locator.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';

class FishDetailPageViewScreen extends StatefulWidget {
  final String currentIndex;
  final String listdata;

  FishDetailPageViewScreen(this.currentIndex, this.listdata);

  @override
  _FishDetailPageViewScreenState createState() => _FishDetailPageViewScreenState();
}

class _FishDetailPageViewScreenState extends State<FishDetailPageViewScreen> {
  PageController controller;
  List<String> list = [];

  @override
  void initState() {
    if (widget.listdata != null) {
      list = (json.decode(widget.listdata) as List)
          .map((e) => e.toString())
          .toList();
    }
    controller = PageController(initialPage: int.parse(widget.currentIndex));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String currentId = list[int.parse(widget.currentIndex)];
    debugPrint(
        'uniqueEntryId: $currentId, index: ${widget.currentIndex}, listdata: ${widget.listdata}');

    return PageView(
      children: _getChildren(),
      controller: controller,
      scrollDirection: Axis.horizontal,
    );
  }

  List<Widget> _getChildren() {
    List<FishScaffold> details = [];
    for (String uniqueId in list) {
      details.add(FishScaffold(
          title: locator<CreatureService>().getFishName(uniqueId),
          child: CreatureDetail(uniqueEntryId: uniqueId)));
    }
    return details;
  }
}
