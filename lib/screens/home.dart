import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/bubble.dart';
import 'package:acnhfishingbuddy/widgets/buttons.dart';
import 'package:acnhfishingbuddy/widgets/homeWidgets.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:acnhfishingbuddy/widgets/topFishes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../locator.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  update() => setState(() => {});

  @override
  void initState() {
    locator<CreatureService>().addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    locator<CreatureService>().removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(
        'build homescreen height: ${MediaQuery.of(context).size.height.toString()}');
    debugPrint(
        'build homescreen width: ${MediaQuery.of(context).size.width.toString()}');

    return FutureBuilder<void>(
        future: locator<CreatureService>().init(context),
        builder: (BuildContext context, AsyncSnapshot snap) {
          return (ConnectionState.done == snap.connectionState)
              ? LoadedHomeScreen() : _getLoading(snap.connectionState);
        });
  }

  Widget _getLoading(ConnectionState state) {
    debugPrint('container ${state.toString()}');
    return Container(
      color: Colors.grey[300],
      child: Center(
        child:
            SizedBox(height: 80, width: 80, child: CircularProgressIndicator()),
      ),
    );
  }
}

class LoadedHomeScreen extends StatefulWidget {
  @override
  _LoadedHomeScreenState createState() => _LoadedHomeScreenState();
}

class _LoadedHomeScreenState extends State<LoadedHomeScreen> {

  @override
  Widget build(BuildContext context) {
    bool animate = locator<CreatureService>().userData?.getAnimatedBackground() ?? true;
    return MainScaffold(
      child: Stack(children: <Widget>[
        Positioned.fill(child: HomeBackground()),
        animate ? Positioned.fill(child: Particles(8)) : Container(),
        Positioned.fill(child: HomeInfoWidget()),
      ]),
    );
  }
}

class HomeInfoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double cardScale = 0.8;
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: FishingThemeData.getCardPaddingFromTop(context)),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.circular(FishingThemeData.getBorderRadius()),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * cardScale,
                  child: FishingThemeData.isBigScreen(context)
                      ? SmallInfoCard()
                      : SmallInfoCard(),
                ),
              ),
            ),
            Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                FishingButton(
                  text: FlutterI18n.translate(context, "doFishing"),
                  onPressed: () => Navigator.pushNamed(context, 'fishing'),
                ),
                SizedBox(
                  height: 20,
                ),
                FishingButton(
                  text: FlutterI18n.translate(context, "todo"),
                  onPressed: () => Navigator.pushNamed(context, 'todo'),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: TopFishesCardWidget(
                dailyCatches:
                    locator<CreatureService>().getDaysCatched(DateTime.now()),
              ),
            ),
            /*FishingButton(
                text: 'Löschen',
                onPressed: () => locator<CreatureService>().deleteCatches(),
              ),*/
            //TapBar(),
            //Container()
          ],
        ),
      ),
    );
  }
}

class HomeBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: FishingThemeData
            .getScaffoldBackground(), //Colors.blueGrey.shade600, //Colors.blueGrey[400],
      ),
    );
  }
}
