import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/widgets/common.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HowtoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FishScaffold(
      title: FlutterI18n.translate(context, 'menu4'),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ColorDescriptionWidget(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: FishingExplainWidget(),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: ShortcutWidget(),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: IconWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FishingExplainWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            children: [
              Text(
                FlutterI18n.translate(context, 'fishingExplainHeader'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1.5,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  FlutterI18n.translate(context, 'fishingExplainHeaderSub'),
                  textScaleFactor: 0.8,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            children: [
              TextLineWidget(
                first: FlutterI18n.translate(context, 'simpleClickAction'),
                desc: FlutterI18n.translate(context, 'simpleClickDesc'),
              ),
              TextLineWidget(
                first: FlutterI18n.translate(context, 'doubleClickAction'),
                desc: FlutterI18n.translate(context, 'doubleClickDesc'),
              ),
              TextLineWidget(
                first: FlutterI18n.translate(context, 'longClickAction'),
                desc: FlutterI18n.translate(context, 'longClickDesc'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}


class IconWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(
            FlutterI18n.translate(context, 'iconHeader'),
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            textScaleFactor: 1.5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconLineWidget(
                icon: Icon(Icons.sort_by_alpha),
                desc: FlutterI18n.translate(context, 'iconABC'),
              ),
              IconLineWidget(
                icon: Center(child: FaIcon(FontAwesomeIcons.exclamation)),
                desc: FlutterI18n.translate(context, 'iconExclamation'),
              ),
              IconLineWidget(
                icon: Icon(Icons.apps),
                desc:  FlutterI18n.translate(context, 'iconGallery'),
              ),
              IconLineWidget(
                icon: Icon(Icons.format_list_bulleted),
                desc:  FlutterI18n.translate(context, 'iconList'),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class IconLineWidget extends StatelessWidget {
  final Widget icon;
  final String desc;

  const IconLineWidget({Key key, this.icon, this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 2,
            child: icon
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 9,
            child: Container(child: Text(desc)),
          )
        ],
      ),
    );
  }
}

class TextLineWidget extends StatelessWidget {
  final String first;
  final String desc;

  const TextLineWidget({Key key, this.first, this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Text(first)
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 8,
            child: Text(desc),
          )
        ],
      ),
    );
  }
}

class ColorDescriptionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            children: [
              Text(
                FlutterI18n.translate(context, 'colorHeader'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1.5,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  FlutterI18n.translate(context, 'colorHeaderSub'),
                  textScaleFactor: 0.8,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            children: [
              ColorLineWidget(
                color: FishingThemeData.getCardBackgroundColorNeverFound(),
                desc: FlutterI18n.translate(context, 'colorNeverFound'),
              ),
              ColorLineWidget(
                color: FishingThemeData.getCardBackgroundColorNotFoundToday(),
                desc: FlutterI18n.translate(context, 'colorTodayNotFound'),
              ),
              ColorLineWidget(
                color: FishingThemeData.getCardBackgroundColor(),
                desc: FlutterI18n.translate(context, 'colorFound'),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class ColorLineWidget extends StatelessWidget {
  final Color color;
  final String desc;

  const ColorLineWidget({Key key, this.color, this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              height: 30,
              decoration: BoxDecoration(
                color: color,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(FishingThemeData.getBorderRadius()),

              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 9,
            child: Text(desc),
          )
        ],
      ),
    );
  }
}

class ShortcutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(children: [
      Padding(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: Text(
          FlutterI18n.translate(context, 'shortcutHeader'),
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textScaleFactor: 1.5,
        ),
      ),
      Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Column(
            children: [
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.Sea),
                value:
                    TranslationHelper.getWhereTranslation(context, Where.Sea),
              ),
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.Pier),
                value:
                    TranslationHelper.getWhereTranslation(context, Where.Pier),
              ),
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.Pond),
                value:
                    TranslationHelper.getWhereTranslation(context, Where.Pond),
              ),
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.River),
                value:
                    TranslationHelper.getWhereTranslation(context, Where.River),
              ),
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.RiverTop),
                value: TranslationHelper.getWhereTranslation(
                    context, Where.RiverTop),
              ),
              ShortcutLineWidget(
                shortcut: TranslationHelper.getWhereShortTranslation(
                    context, Where.RiverMouth),
                value: TranslationHelper.getWhereTranslation(
                    context, Where.RiverMouth),
              ),
            ],
          )),
    ]);
  }
}

class ShortcutLineWidget extends StatelessWidget {
  final String shortcut;
  final String value;

  const ShortcutLineWidget({Key key, this.shortcut, this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(shortcut),
          ),
          Expanded(
            flex: 1,
            child: Text(" - "),
          ),
          Expanded(
            flex: 9,
            child: Text(value),
          )
        ],
      ),
    );
  }
}
