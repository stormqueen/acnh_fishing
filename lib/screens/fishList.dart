import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';

import '../locator.dart';

class FishListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Creature> creatures = locator<CreatureService>().getFishes();
    List<String> ids = creatures.map((e) => e.uniqueEntryId).toList();
    Widget child = ListView.builder(
        itemCount: creatures.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: CreatureListTile(creature: creatures[index], list: ids,),
          );
        });
    return FishScaffold(child: child, title: 'Alle Fische');
  }
}
