import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../locator.dart';

const int settingFlex1 = 5;

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FishScaffold(
      title: FlutterI18n.translate(context, 'menu2'),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Center(
          child: Column(
            children: [
              SettingsWrapperWidget(UserSettingsWidget()),
              SettingsWrapperWidget(OtherSettingsWidget())
            ],
          ),
        ),
      ),
    );
  }
}

class SettingsWrapperWidget extends StatelessWidget {
  final Widget child;

  const SettingsWrapperWidget(this.child);

  @override
  Widget build(BuildContext context) {
    double cardScale = 0.9;
    return Padding(
      padding:
          EdgeInsets.only(top: FishingThemeData.getCardPaddingFromTop(context)),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(FishingThemeData.getBorderRadius()),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * cardScale,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: child,
          ),
        ),
      ),
    );
  }
}

class OtherSettingsWidget extends StatefulWidget {
  @override
  _OtherSettingsWidgetState createState() => _OtherSettingsWidgetState();
}

class _OtherSettingsWidgetState extends State<OtherSettingsWidget> {

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(FlutterI18n.translate(context, 'otherSettings')),
              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.settings,
                  color: FishingThemeData.getFirsColor(),
                ),
              ],
            ),
          ],
        ),
      ),
      Divider(),
      Row(
        children: [
          Expanded(
            flex: settingFlex1,
            child: Text(FlutterI18n.translate(context, 'sortAbc')),
          ),
          Expanded(
            flex: 12 - settingFlex1,
            child: Row(
              children: [
                Switch(
                  value: !locator<CreatureService>().userData.getSortSpawn(),
                  onChanged: (value) => setState(() {
                    locator<CreatureService>().changeSortSpawn(value);
                  }),
                ),
              ],
            ),
          ),
        ],
      ),
      Row(
        children: [
          Expanded(flex: 12, child: Text(FlutterI18n.translate(context, 'otherwisesortspawn'), textScaleFactor: 0.8,)),
        ],
      ),
      Row(
        children: [
          Expanded(
            flex: settingFlex1,
            child: Text(FlutterI18n.translate(context, 'animateBackground')),
          ),
          Expanded(
            flex: 12 - settingFlex1,
            child: Row(
              children: [
                Switch(
                  value: locator<CreatureService>().userData.getAnimatedBackground(),
                  onChanged: (value) => setState(() {
                    locator<CreatureService>().changeAnimatedBackground(value);
                  }),
                ),
              ],
            ),
          ),
        ],
      ),
    ]);
  }
}

class UserSettingsWidget extends StatefulWidget {
  @override
  _UserSettingsWidgetState createState() => _UserSettingsWidgetState();
}

class _UserSettingsWidgetState extends State<UserSettingsWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController islandController = TextEditingController();

  void _saveUserData() {
    locator<CreatureService>().userData.name = nameController.text;
    locator<CreatureService>().userData.islandName = islandController.text;
    locator<CreatureService>().saveUserData();
    debugPrint("save userdata");
  }

  @override
  void initState() {
    nameController.addListener(_saveUserData);
    islandController.addListener(_saveUserData);
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    islandController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String userName = locator<CreatureService>().userData.name;
    String islandName = locator<CreatureService>().userData.islandName;
    nameController.text = userName;
    islandController.text = islandName;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(FlutterI18n.translate(context, 'islandSettings')),
                ],
              ),
              Row(
                children: [
                  FaIcon(
                    FontAwesomeIcons.leaf,
                    color: FishingThemeData.getFirsColor(),
                  ),
                ],
              ),
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.only(bottom: 18.0, top: 10),
          child: Row(
            children: [
              Expanded(
                flex: settingFlex1,
                child: Text(FlutterI18n.translate(context, 'name')),
              ),
              Expanded(
                flex: 12 - settingFlex1,
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: FlutterI18n.translate(context, 'nameDesc'),
                  ),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 28.0),
          child: Row(
            children: [
              Expanded(
                flex: settingFlex1,
                child: Text(FlutterI18n.translate(context, 'islandName')),
              ),
              Expanded(
                flex: 12 - settingFlex1,
                child: TextField(
                  controller: islandController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: FlutterI18n.translate(context, 'islandNameDesc'),
                  ),
                ),
              )
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              flex: settingFlex1,
              child: Text(FlutterI18n.translate(context, 'north')),
            ),
            Expanded(
              flex: 12 - settingFlex1,
              child: Row(
                children: [
                  Switch(
                    value: locator<CreatureService>().userData.getNorth(),
                    onChanged: (value) => setState(() {
                      locator<CreatureService>().changeThern(value);
                    }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
