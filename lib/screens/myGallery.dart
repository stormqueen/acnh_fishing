import 'dart:convert';

import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/provider/myGalleryProvider.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';

import '../locator.dart';

class MyGalleryScreen extends StatefulWidget {

  @override
  _MyGalleryScreenState createState() => _MyGalleryScreenState();
}

class _MyGalleryScreenState extends State<MyGalleryScreen> {

  @override
  Widget build(BuildContext context) {
    bool bigHeight = MediaQuery.of(context).size.height > 800;

    return ChangeNotifierProvider(
      create: (_) => MyGalleryProvider(locator<CreatureService>().fishes, locator<CreatureService>().myFishes, locator<CreatureService>().userData.getNorth(), !locator<CreatureService>().userData.getSortSpawn()),
      builder: (context, child) {
        return MyGalleryScaffold(
          title: FlutterI18n.translate(context, "of",
              translationParams: {
                'x': context.watch<MyGalleryProvider>().getDifferentCatchedFishes().toString(),
                'y': context.watch<MyGalleryProvider>().getFilteredCreatures().length.toString()
              }),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                flex: context.watch<MyGalleryProvider>().editMode && context.watch<MyGalleryProvider>().showTip ? bigHeight ? 1 : 2 : 0,
                child: context.watch<MyGalleryProvider>().editMode && context.watch<MyGalleryProvider>().showTip ? MyFishesTipWidget() : SizedBox(height: 0,child: Container()),
              ),
              Expanded(
                flex: context.watch<MyGalleryProvider>().editMode && context.watch<MyGalleryProvider>().showTip ? bigHeight ? 11 : 10 : 12,
                child: MyGalleryList(),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            child: context.watch<MyGalleryProvider>().editMode ? Icon(Icons.cancel, color: FishingThemeData.getSecondColor(),) : Icon(Icons.mode_edit, color: FishingThemeData.getSecondColor()),
            onPressed: () => context.read<MyGalleryProvider>().swapEditMode(),
          ),
        );
      },
    );
  }
}

class MyGalleryList extends StatelessWidget {

  Widget _getGrid(BuildContext context) {
    int tiles = MediaQuery.of(context).size.width > 460 ? 4 : 2;
    double ratio = 1;

    return GridView.count(
      shrinkWrap: true,
      padding: EdgeInsets.all(4),
      crossAxisCount: tiles,
      childAspectRatio: ratio,
      children: List.generate(
        context.watch<MyGalleryProvider>().getFilteredCreatures().length,
            (index) {
          return FishingTileWidget(
              creature: context.watch<MyGalleryProvider>().getFilteredCreatures()[index],
              tiles: tiles,
              ratio: ratio
          );
        },
      ),
    );
  }

  Widget _getList(BuildContext context) {
    return ListView.builder(
        itemCount: context.watch<MyGalleryProvider>().getFilteredCreatures().length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: FishingListWidget(creature: context.watch<MyGalleryProvider>().getFilteredCreatures()[index]),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    bool listView = context.watch<MyGalleryProvider>().viewModeList;
    return listView ? _getList(context) : _getGrid(context);

  }
}

class FishingListWidget extends StatelessWidget {
  final Creature creature;

  const FishingListWidget({Key key, this.creature}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String unique = creature.uniqueEntryId;
    List<String> ids = context.watch<MyGalleryProvider>().getFilteredCreatures().map((e) => e.uniqueEntryId).toList();
    int index = ids.indexOf(unique);
    String list = json.encode(ids);

    bool neverfound = !context.watch<MyGalleryProvider>().isCatchted(creature.uniqueEntryId);
    String catchTimes = locator<CreatureService>().myFishes.getAllCatchesForFish(creature.uniqueEntryId).toString();

    return GestureDetector(
      onTap: () => context.read<MyGalleryProvider>().editMode ? context.read<MyGalleryProvider>().swapCatched(creature.uniqueEntryId) : Navigator.pushNamed(context, 'detail/$unique/$index/$list'),
      child: Container(
        decoration: BoxDecoration(
          color: neverfound ? FishingThemeData.getCardBackgroundColorNeverFound(): FishingThemeData.getCardBackgroundColor(),
          borderRadius: BorderRadius.circular(FishingThemeData.getBorderRadius()),
        ),
        child: ListTile(
          leading: CircleAvatar(
            child: Image.network(
              context.watch<MyGalleryProvider>().editMode ? creature.iconImage : creature.critterpediaImage,
            ),
          ),
          title: Row(
            children: [
              Text(creature.translation),
              Text(' ($catchTimes)')
            ],
          ),
          subtitle: Row(
            children: [
              Text(TranslationHelper.getWhereTranslation(context, creature.where)),
              FishSize(size: creature.shadowSize)
            ],
          ),
          trailing: Icon(neverfound ? Icons.remove_circle_outline : Icons.check_circle_outline, size: 24, color: neverfound ? Colors.grey : Colors.green,),
        ),
      ),
    );
  }
}

class FishingTileWidget extends StatelessWidget {
  final int tiles;
  final double ratio;
  final Creature creature;

  const FishingTileWidget({Key key, @required this.creature, @required this.tiles, @required this.ratio}) : super(key: key);

  Widget getContent(BuildContext context) {
    int flex = 3;
    double scale = 1/ratio;
    double size = (((MediaQuery.of(context).size.width -(tiles*16)-(tiles*16)) / tiles)*scale)*1;
    //debugPrint('size: $size');

    bool neverfound = !context.watch<MyGalleryProvider>().isCatchted(creature.uniqueEntryId);

    String unique = creature.uniqueEntryId;
    List<String> ids = context.watch<MyGalleryProvider>().getFilteredCreatures().map((e) => e.uniqueEntryId).toList();
    int index = ids.indexOf(unique);
    String list = json.encode(ids);

    String catchTimes = locator<CreatureService>().myFishes.getAllCatchesForFish(creature.uniqueEntryId).toString();

    return GestureDetector(
      onTap: () => context.read<MyGalleryProvider>().editMode ? context.read<MyGalleryProvider>().swapCatched(creature.uniqueEntryId) : Navigator.pushNamed(context, 'detail/$unique/$index/$list'),
      child: Card(
        color: neverfound ? FishingThemeData.getCardBackgroundColorNeverFound(): FishingThemeData.getCardBackgroundColor(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 12,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          flex: 12-flex,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  creature.translation,
                                  textScaleFactor: FishingThemeData.getInfoTextSize(context),
                                ),
                                Row(
                                  children: [
                                    FishSize(size: creature.shadowSize)
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        //Icon(Icons.add)
                        Expanded(
                          flex: flex,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Icon(neverfound ? Icons.remove_circle_outline : Icons.check_circle_outline, size: 24, color: neverfound ? Colors.grey : Colors.green,)
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            context.watch<MyGalleryProvider>().editMode ? creature.iconImage : creature.critterpediaImage,
                            width: size,
                            height: size,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0, right: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [

                        Text(FlutterI18n.translate(context, 'catchTimes', translationParams: {'times': catchTimes}),
                          textScaleFactor: 1.3, style: TextStyle(color: FishingThemeData.getFirsColor()),)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return getContent(context);
  }
}

class MyFishesTipWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.circular(FishingThemeData.getBorderRadius()),
        ),
        child: Row(
          children: [
          Expanded(
            flex: 10,
            child: Padding(
              padding: const EdgeInsets.only(left: 18.0),
              child: Text(FlutterI18n.translate(context, 'myGalleryEditTip'), style: TextStyle(color: FishingThemeData.getTipColor()),),
            ),
          ),
            Expanded(
              flex: 2,
              child: IconButton(
                icon: Icon(Icons.cancel, color: FishingThemeData.getFirsColor(),),
                onPressed: () => context.read<MyGalleryProvider>().cancelShowTip(),
              ),
            ),
          ]
        ),
      ),
    );
  }
}
