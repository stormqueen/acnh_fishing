import 'dart:async';

import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

class PurchaseScreen extends StatefulWidget {
  @override
  _PurchaseScreenState createState() => _PurchaseScreenState();
}

class _PurchaseScreenState extends State<PurchaseScreen> {
  final Set<String> existingProductIds = ['1', '2', '3', '4'].toSet() ;

  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;

  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<ProductDetails> _products = [];

  String message;
  bool loading = true;
  bool success = false;

  //debugStuff
  String debugMessage = "";
  List<String> pastPurchases = [];

  Future<void> _initStoreInfo() async {
    debugPrint('initStoreInfo');
    final bool isAvailable = await _connection.isAvailable();
    if (!isAvailable) {
      setState(() {
        _products = [];
        message = "";
      });
      return;
    }
    debugMessage += " productsToQuery: " + existingProductIds.toString();
    ProductDetailsResponse productDetailResponse = await _connection
        .queryProductDetails(existingProductIds);
    if (productDetailResponse.error != null) {
      debugMessage += " productDetailResponse.error: " +
          productDetailResponse.error.toString();
      setState(() {
        //_queryProductError = productDetailResponse.error.message;
        _products = productDetailResponse.productDetails;
        loading = false;
        //_notFoundIds = productDetailResponse.notFoundIDs;
      });
      return;
    }

    if (productDetailResponse.productDetails.isNotEmpty) {
      debugMessage += " productDetails.isNotEmpty ";
      setState(() {
        // _queryProductError = null;
        _products = productDetailResponse.productDetails;
        loading = false;
        debugMessage += ' not found ids: ' + productDetailResponse.notFoundIDs.toString();
      });
    }

    final QueryPurchaseDetailsResponse purchaseResponse = await _connection
        .queryPastPurchases();
    if (purchaseResponse.error != null) {
      // handle query past purchase error..
    }

    if (purchaseResponse.pastPurchases != null &&
        purchaseResponse.pastPurchases.isNotEmpty) {
      for (PurchaseDetails p in purchaseResponse.pastPurchases) {
        pastPurchases.add(p.productID);
      }
    }
  }

  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
    success = false;
    for (PurchaseDetails d in purchaseDetailsList) {
      debugPrint("purchase ${d.status.toString()}");
      debugMessage = "purchaseStatus: ${d.status.toString()}";
      if (d.status == PurchaseStatus.purchased) {
        InAppPurchaseConnection.instance.consumePurchase(d);
        setState(() {
          message = FlutterI18n.translate(context, 'thanksForPurchase');
          success = true;
        });
      } else if (d.status == PurchaseStatus.error) {
        setState(() {
          message = FlutterI18n.translate(context, 'errorDuringPurchase');
        });
      }
    }

  }

  @override
  void initState() {
    Stream purchaseUpdated = InAppPurchaseConnection.instance.purchaseUpdatedStream;
    _subscription = purchaseUpdated.listen((purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      _subscription.cancel();
      message = "_subscription.cancel";
      setState(() {});
    }, onError: (error) {
      //message = FlutterI18n.translate(context, "errorDuringPurchase", Map.fromIterables(["theme"], [""]));
      setState(() {});
    });
    _initStoreInfo();
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FishScaffold(
      title: FlutterI18n.translate(context, 'purchaseHeader'),
      child: loading ? Center(child: CircularProgressIndicator()) : Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ProductListWidget(_products, debugMessage, message, success),
          Container(
            color: FishingThemeData.getCardBackgroundColor(),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8, top: 8, bottom: 18),
              child: Row(
                children: [
                  Expanded(flex: 2, child: FaIcon(FontAwesomeIcons.moneyBill, color: FishingThemeData.getFirsColor(),),),
                  Expanded(flex: 10, child: Text(FlutterI18n.translate(context, 'supportInfo'))),
                ],
              ),
            ),
          )
        ],
      )
    );
  }
  
}

class ProductListWidget extends StatefulWidget {
  final List<ProductDetails> _products;
  final String debugMessage;
  final String message;
  final bool success;

  const ProductListWidget(this._products, this.debugMessage, this.message, this.success);

  @override
  _ProductListWidgetState createState() => _ProductListWidgetState();
}

class _ProductListWidgetState extends State<ProductListWidget> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //!kReleaseMode ? Text(widget.debugMessage) : Container(),
        widget.message != null && widget.message.isNotEmpty ? SupportThanksWidget(text: widget.message, success: widget.success,) : Container(),
        Padding(
          padding: const EdgeInsets.only(top: 18.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            child: ListView.builder(
                padding: EdgeInsets.only(bottom: 8),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: widget._products.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: FishingThemeData.getCardBackgroundColor(),
                          borderRadius:
                          BorderRadius.circular(FishingThemeData.getBorderRadius(),),),

                      child: ListTile(
                       // leading: Icon(Icons.attach_money),
                        title: Text(widget._products[index].description),
                       // subtitle: Text(widget._products[index].description),
                        trailing: Container(
                          width: 100,
                          child: !loading ? RaisedButton(
                            onPressed: () {
                              final PurchaseParam purchaseParam = PurchaseParam(productDetails: widget._products[index]);
                              InAppPurchaseConnection.instance
                                  .buyNonConsumable(
                                purchaseParam: purchaseParam,
                              )
                                  .whenComplete(() {
                                setState(() {
                                  loading = false;
                                });
                              });
                              setState(() {
                                loading = true;
                              });
                            },
                            child: Text(widget._products[index].price),
                          ) : CircularProgressIndicator(),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}

class SupportThanksWidget extends StatelessWidget {

  final String text;
  final bool success;

  const SupportThanksWidget({Key key, this.text, this.success}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        decoration: BoxDecoration(
          color: FishingThemeData.getCardBackgroundColor(),
          borderRadius:
          BorderRadius.circular(FishingThemeData.getBorderRadius(),),),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(children: [
              Expanded(flex: 2, child: FaIcon(success ? FontAwesomeIcons.smileBeam : FontAwesomeIcons.sadTear, color: FishingThemeData.getFirsColor(),)),
              Expanded(flex: 10, child: Text(text)),
          ],)
        ),
      ),
    );
  }
}
