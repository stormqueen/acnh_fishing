import 'dart:convert';

import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/locator.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/provider/filter.dart';
import 'package:acnhfishingbuddy/provider/todoProvider.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => TodoProvider(
            locator<CreatureService>().myFishes,
            locator<CreatureService>().fishes,
            locator<CreatureService>().userData.getNorth(),
            !locator<CreatureService>().userData.getSortSpawn()),
        builder: (context, child) {
          return FishScaffold(
              title: FlutterI18n.translate(context, 'menu5'),
              child: SingleChildScrollView(
                primary: true,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                        ChoiceChip(
                          selectedColor: FishingThemeData.getFirsColor(),
                          labelStyle: TextStyle(color: Colors.black),
                          selected: context
                              .watch<TodoProvider>()
                              .isSelected(FishFilterValue.NowAvailable),
                          onSelected: (value) =>
                              context.read<TodoProvider>().switchNow(value),
                          label: Row(children: [
                            Text(
                              TranslationHelper.getFishFilterTranslation(
                                  context, FishFilterValue.NowAvailable) +' ('+context.watch<TodoProvider>().nowCatchable.toString()+')',
                            )
                          ]),
                        ),
                        ChoiceChip(
                          selectedColor: FishingThemeData.getFirsColor(),
                          labelStyle: TextStyle(color: Colors.black),
                          selected: context
                              .watch<TodoProvider>()
                              .isSelected(FishFilterValue.NowNotAvailable),
                          onSelected: (value) =>
                              context.read<TodoProvider>().switchNotNow(value),
                          label: Row(children: [
                            Text(
                              TranslationHelper.getFishFilterTranslation(
                                  context, FishFilterValue.NowNotAvailable) +' ('+context.watch<TodoProvider>().nowNotCatchable.toString()+')',
                            )
                          ]),
                        ),
                      ],),
                      TodoContainerWidget(),
                    ],
                  ),
                ),
              ));
        });
  }
}

class TodoContainerWidget extends StatelessWidget {
  Widget _getList(BuildContext context) {
    return  context
        .watch<TodoProvider>()
        .getFilteredCreatures().length > 0 ? ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: context.watch<TodoProvider>().getFilteredCreatures().length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child:TodoListWidget(
                creature: context
                    .watch<TodoProvider>()
                    .getFilteredCreatures()[index])
          );
        }) : _getNoFishesWithFilter(context);
  }

  Widget _getNoFishesWithFilter(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 18.0, top: 18),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            FlutterI18n.translate(context, 'noFishesWithFilter'),
            textAlign: TextAlign.center,
            textScaleFactor: 1.1,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: FaIcon(
              FontAwesomeIcons.questionCircle,
              color: FishingThemeData.getFirsColor(),
              size: 40,
            ),
          )
        ],
      ),
    );
  }

  Widget _getNothingTodo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 18.0, top: 18),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            FlutterI18n.translate(context, 'nothingTODO'),
            textAlign: TextAlign.center,
            textScaleFactor: 1.1,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: Icon(
              Icons.check_circle_outline,
              color: FishingThemeData.getFirsColor(),
              size: 40,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.95,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(FishingThemeData.getBorderRadius()),
              topRight: Radius.circular(FishingThemeData.getBorderRadius()),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: FishingThemeData.getFirsColor(),
                    borderRadius: BorderRadius.only(
                      topLeft:
                          Radius.circular(FishingThemeData.getBorderRadius()),
                      topRight:
                          Radius.circular(FishingThemeData.getBorderRadius()),
                    )),
                child: ListTile(
                  //leading: Icon(Icons.format_list_numbered, color: FishingThemeData.getSecondColor(),),
                  leading: Icon(Icons.today,
                      color: FishingThemeData.getSecondColor()),
                  title: Text(
                    FlutterI18n.translate(context, 'todoHeader') +
                        ' (' +
                        context
                            .watch<TodoProvider>()
                            .toCatchThisMonth
                            .toString() +
                        ')',
                    // textScaleFactor: 1.5,
                    style: TextStyle(
                      color: FishingThemeData.getSecondColor(),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: context
                            .watch<TodoProvider>()
                            .allCatchedThisMonth
                    ? _getNothingTodo(context)
                    : _getList(context),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TodoListWidget extends StatelessWidget {
  final Creature creature;

  const TodoListWidget({Key key, this.creature}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String unique = creature.uniqueEntryId;
    List<String> ids = context
        .watch<TodoProvider>()
        .getFilteredCreatures()
        .map((e) => e.uniqueEntryId)
        .toList();
    int index = ids.indexOf(unique);
    String list = json.encode(ids);
    bool lastMonth = context.watch<TodoProvider>().isLastMonth(creature);
    bool north = context.watch<TodoProvider>().north;

    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, 'detail/$unique/$index/$list'),
      child: Container(
        decoration: BoxDecoration(
          color: FishingThemeData.getCardBackgroundColorNeverFound(),
          borderRadius:
              BorderRadius.circular(FishingThemeData.getBorderRadius()),
        ),
        child: ListTile(
          leading: CircleAvatar(
            child: Image.network(
              creature.critterpediaImage,
            ),
          ),
          title: Row(
            children: [
              Text(creature.translation),
              Text(' (' + creature.spawnRates + ' %)')
            ],
          ),
          subtitle: Column(
            children: [
              Row(
                children: [
                  Text(TranslationHelper.getWhereTranslation(
                      context, creature.where)),
                  FishSize(size: creature.shadowSize)
                ],
              ),
              Row(
                children: [
                  Text(FlutterI18n.translate(context, 'fishingTime') + ': '),
                  CreatureTimeWidget(
                      thern: creature.getCurrentThern(
                          north, DateTime.now().month)),
                ],
              )
            ],
          ),
          trailing: lastMonth
              ? FaIcon(FontAwesomeIcons.exclamation)
              : Container(
                  width: 0,
                  height: 0,
                ),
        ),
      ),
    );
  }
}
