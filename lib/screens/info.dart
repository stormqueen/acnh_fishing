import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/widgets/common.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FishScaffold(
      title: FlutterI18n.translate(context, 'menu3'),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AboutWidget(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: DataSourceWidget(),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: PrivacyWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(
            FlutterI18n.translate(context, 'infoeHeader'),
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            textScaleFactor: 1.5,
          ),
        ),Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(FlutterI18n.translate(context, 'info1')),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(FlutterI18n.translate(context, 'info2')),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 30.0),
          child: Text(FlutterI18n.translate(context, 'info3')),
        ),
        FutureBuilder(
          future: PackageInfo.fromPlatform(),
          builder: (context, snap) {
            if (snap.connectionState == ConnectionState.done &&
                snap.hasData) {
              PackageInfo packageInfo = snap.data;

              return InkWell(
                child: Text(
                  FlutterI18n.translate(context, 'moreInfo'),
                  style:
                  TextStyle(color: FishingThemeData.getLinkColor()),
                ),
                onTap: () => showAboutDialog(
                    context: context,
                    applicationName:
                    FlutterI18n.translate(context, 'appName'),
                    applicationVersion: packageInfo.version,
                    applicationIcon: Image(
                      image: AssetImage('assets/images/appIcon.png'),
                      height: 50,
                    )),
              );
            }
            return Container();
          },
        ),
      ],
    );
  }
}

class DataSourceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(
            FlutterI18n.translate(context, 'infoSourceHeader'),
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            textScaleFactor: 1.5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(FlutterI18n.translate(context, 'infoSource1')),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: InkWell(
            child: Text(
              FlutterI18n.translate(context, 'infoUrlData'),
              style: TextStyle(color: FishingThemeData.getLinkColor()),
            ),
            onTap: () => launch(FlutterI18n.translate(context, 'infoUrlData')),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(FlutterI18n.translate(context, 'infoSource2')),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 30.0),
          child: InkWell(
            child: Text(
              FlutterI18n.translate(context, 'infoUrlTranslation'),
              style: TextStyle(color: FishingThemeData.getLinkColor()),
            ),
            onTap: () =>
                launch(FlutterI18n.translate(context, 'infoUrlTranslation')),
          ),
        ),
      ],
    );
  }
}

class PrivacyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoCardWidget(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(
            FlutterI18n.translate(context, 'infoPrivacyHeader'),
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            textScaleFactor: 1.5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: Text(FlutterI18n.translate(context, 'infoPrivacyText')),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 30.0),
          child: InkWell(
            child: Text(
              FlutterI18n.translate(context, 'infoPrivacyLinkText'),
              style: TextStyle(color: FishingThemeData.getLinkColor()),
            ),
            onTap: () =>
                launch(FlutterI18n.translate(context, 'infoPrivacyLink')),
          ),
        ),

      ],
    );
  }
}