import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/provider/fishingProvider.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:acnhfishingbuddy/widgets/creatureWidget.dart';
import 'package:acnhfishingbuddy/widgets/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wakelock/wakelock.dart';

import '../locator.dart';

class FishingScreen extends StatefulWidget {
  @override
  _FishingScreenState createState() => _FishingScreenState();
}

class _FishingScreenState extends State<FishingScreen> {

  @override
  void initState() {
    Wakelock.enable();
    debugPrint('wakelock enabled');
    super.initState();
  }

  @override
  void dispose() {
    Wakelock.disable();
    debugPrint('wakelock disabled');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int tiles = MediaQuery.of(context).size.width > 460 ? 3 : 2;
    double ratio = 2.5;
    DateTime now = DateTime.now();
    return ChangeNotifierProvider(
      create: (_) => FishingProvider(
          locator<CreatureService>().myFishes,
          locator<CreatureService>().getCurrentFishes(now.month, now.hour),
          locator<CreatureService>().getDaysCatched(now),
          !locator<CreatureService>().userData.getSortSpawn()),
      builder: (context, child) {
        return FishingScaffold(
          child: GridView.count(
            padding: EdgeInsets.all(4),
            crossAxisCount: tiles,
            childAspectRatio: ratio,
            children: List.generate(
              context.watch<FishingProvider>().creatures.length,
              (index) {
                return FishingTileWidget(
                  creature: context.watch<FishingProvider>().creatures[index],
                  tiles: tiles,
                  ratio: ratio
                );
              },
            ),
          ),
        );
      },
    );
  }
}

class FishingTileWidget extends StatelessWidget {
  final int tiles;
  final double ratio;
  final Creature creature;

  const FishingTileWidget({Key key, @required this.creature, @required this.tiles, @required this.ratio}) : super(key: key);

  Widget getContent(BuildContext context) {
    double scale = 1/ratio;
    double size = ((MediaQuery.of(context).size.width -(tiles*16)-(tiles*16)) / tiles)*scale;
    int catchCounter = context.watch<FishingProvider>().getCatchCounter(creature.uniqueEntryId);
    bool neverfound = context.watch<FishingProvider>().neverFound(creature.uniqueEntryId);
    double counterScale = catchCounter < 10 ? 2 : 1;
    int flex = catchCounter < 10 ? 1 : 2;
    debugPrint('size: $size');
    return GestureDetector(
      onLongPress: () => context.read<FishingProvider>().removeCatched(creature.uniqueEntryId),
      onTap: () => context.read<FishingProvider>().addCatched(creature.uniqueEntryId),
      onDoubleTap: () => Navigator.pushNamed(context, 'detail/${creature.uniqueEntryId}'),
      child: Card(
        color: catchCounter == 0 ? neverfound ? FishingThemeData.getCardBackgroundColorNeverFound(): FishingThemeData.getCardBackgroundColorNotFoundToday() : FishingThemeData.getCardBackgroundColor(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 12,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          flex: 12-flex,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  creature.translation,
                                  textScaleFactor: FishingThemeData.getInfoTextSize(context),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      ' ('+creature.sell.toString()+')',
                                      textScaleFactor: FishingThemeData.getInfoTextSize(context)*0.8,
                                    ),
                                    FishSize(size: creature.shadowSize),
                                    Text(
                                      ' ('+TranslationHelper.getWhereShortTranslation(context, creature.where)+')',
                                      textScaleFactor: FishingThemeData.getInfoTextSize(context)*0.8,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        //Icon(Icons.add)
                        Expanded(
                          flex: flex,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              catchCounter.toString(),
                              textScaleFactor: counterScale,
                              style: TextStyle(color: FishingThemeData.getFirsColor(),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            creature.critterpediaImage,
                            width: size,
                            height: size,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return getContent(context);
  }
}
