import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:acnhfishingbuddy/model/translation.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

import 'package:flutter/services.dart';

class JsonHelper {
  static Future<List<Translation>> loadTranslations() async {
    String jsonString = await loadFile('assets/fishtranslation.json');
    return decodeTranslations(jsonString);
  }

  static Future<List<Creature>> loadCreatures() async {
    String jsonString = await loadFile('assets/creatures.json');
    return decodeCreatures(jsonString);
  }

  static List<Creature> decodeCreatures(String jsonString) {
    var tmp = json.decode(jsonString) as List;
    List<Creature> result = tmp.map((i) => Creature.fromJson(i)).toList();
    debugPrint("decode ${result.length} creatures");
    return result;
  }

  static List<Translation> decodeTranslations(String jsonString) {
    var tmp = json.decode(jsonString) as List;
    List<Translation> result = tmp.map((i) => Translation.fromJson(i)).toList();
    return result;
  }

  static List<DailyCatches> decodeDailyCatches(String jsonString) {
    var tmp = json.decode(jsonString) as List;
    List<DailyCatches> result = tmp.map((i) => DailyCatches.fromJson(i)).toList();
    debugPrint(result[0].catches.length.toString());
    return result;
  }

  static String encodeDailyCatches(List<DailyCatches> dailyCatches) {
    String jsonString = jsonEncode(dailyCatches.map((e) => e.toJson()).toList());
    return jsonString;
  }

  static Map<String, bool> decodeCatched(String jsonString) {
    var tmp = json.decode(jsonString) as Map;
    Map<String, bool> result = Map();
    tmp.forEach((key, value) {result[key] = value; });
    debugPrint(tmp.length.toString());
    return result;
  }

  static String encodeCatched(Map<String, bool> catched) {
    String jsonString = jsonEncode(catched);
    debugPrint('encodeCatched: $jsonString');
    return jsonString;
  }

  static UserData decodeUserData(String jsonString) {
    var tmp = json.decode(jsonString) as Map;
    UserData result = UserData.fromJson(tmp);
    return result;
  }

  static String encodeUserData(UserData data) {
    String jsonString = jsonEncode(data.toJson());
    debugPrint('encodeUserData: $jsonString');
    return jsonString;
  }


  static Future<String> loadFile(String filePath) async {
    if (filePath != null && filePath.isNotEmpty) {
      final json = await rootBundle.loadString(filePath);
      return json;
    }
    return null;
  }
}