import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'jsonHelper.dart';

class DataService {
  static const PREF_KEY_FISH_CATCHES = "FISH_CATCHES";
  static const PREF_KEY_FISH_CATCHED = "FISH_CATCHED";
  static const PREF_KEY_USERDATA = "USERDATA";

  DataService();


  Future<Catches> deleteCatches() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    prefs.remove(PREF_KEY_FISH_CATCHES);
    return loadCatches();
  }

  Future<UserData> loadUserData() async {
    UserData data;
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    String jsonString = prefs.getString(PREF_KEY_USERDATA);
    if (jsonString != null) {
      data = JsonHelper.decodeUserData(jsonString);
      debugPrint('userData loaded');
    } else {
      data = UserData();
      debugPrint('no userData found');
    }
    return data;
  }

  Future<void> saveUserData(UserData userData) async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;

    String stringToSave = JsonHelper.encodeUserData(userData);
    await prefs.setString(PREF_KEY_USERDATA, stringToSave);
    debugPrint('userData saved');

  }

  Future<Catches> loadCatches() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;

    String jsonString = prefs.getString(PREF_KEY_FISH_CATCHES);
    String jsonString1 = prefs.getString(PREF_KEY_FISH_CATCHED);
    Catches catches = Catches([], Map());
    if (jsonString != null) {
      List<DailyCatches> daily = JsonHelper.decodeDailyCatches(jsonString);
      catches.dailyCatches.addAll(daily);
      debugPrint('loaded ${catches.dailyCatches.length.toString()} days');
    }
    if (jsonString1 != null) {
      Map<String, bool> catched = JsonHelper.decodeCatched(jsonString1);
      catches.catched.addAll(catched);
      debugPrint('loaded ${catches.catched.length.toString()} catched fished');
    }
    return catches;
  }

  Future<void> saveCatched(Catches catches) async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;

    String stringToSave = JsonHelper.encodeDailyCatches(catches.dailyCatches);
    await prefs.setString(PREF_KEY_FISH_CATCHES, stringToSave);
    debugPrint('catches saved');
    //calculate catches
    catches.getAllFishUniqueIds().forEach((element) {
      if (!catches.catched.containsKey(element)) {
        int c = catches.getAllCatchesForFish(element);
        if (c > 0) {
          catches.catched[element] = true;
        } else {
          catches.catched[element] = false;
        }
      } else if (catches.catched[element] == false) {
        //do not reset a fish that is set to true!
        int c = catches.getAllCatchesForFish(element);
        if (c > 0) {
          catches.catched[element] = true;
        }
      }
    });
    String stringToSave1 = JsonHelper.encodeCatched(catches.catched);
    await prefs.setString(PREF_KEY_FISH_CATCHED, stringToSave1);
    debugPrint('catched saved');
  }

}
