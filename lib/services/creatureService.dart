import 'dart:math';

import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl_standalone.dart';

import 'dataService.dart';
import 'jsonHelper.dart';

class CreatureService extends ChangeNotifier {
  static const PREF_KEY_FISH_CATCHES = "FISH_CATCHES";
  static const String LOCALE_SEPARATOR = "_";

  DataService dataService;

  //saved data
  Catches myFishes;
  UserData userData;

  List<Creature> fishes = [];

  CreatureService() {
    debugPrint('create CreatureService');
    userData = new UserData();
  }

  Locale _toLocale(final String locale) {
    final List<String> systemLocaleSplitted = locale.split(LOCALE_SEPARATOR);
    final bool noCountryCode = systemLocaleSplitted.length == 1;
    return Locale(systemLocaleSplitted.first,
        noCountryCode ? null : systemLocaleSplitted.last);
  }

  Future<void> init(BuildContext context) async {
    myFishes = Catches([], Map());
    dataService = DataService();

    String locale =  await findSystemLocale();
    Locale l = _toLocale(locale);
    debugPrint('localeString: $locale, locale: $l');
    if (locale.startsWith('de')) {
      FlutterI18n.refresh(context, l);
    }
    //await flutterI18nDelegate.load(l);
    await setCreatureData(locale);
    await setCatchedData();
    await setUserData();
    //await dummy();
  }

  Future<bool> dummy() async {
    await Future.delayed(Duration(seconds: 5), () {
      debugPrint('dummy complete');
      return true;
    });
    return false;
  }

  Future<void> setCreatureData(String locale) async {
    await JsonHelper.loadCreatures().then((value) {
      fishes = value.where((element) => SourceSheet.Fish == element.sourceSheet).toList();
      debugPrint("load ${fishes.length} fishes");
    });
    await JsonHelper.loadTranslations().then((value) {
      value.forEach((element) {
        Creature creature = fishes.firstWhere((c) => c.name == element.ref);
        if (element.localization.containsKey(locale)) {
          creature.translation = element.localization[locale];
          creature.translated = true;
        } else {
          creature.translation = element.localization['en_US'];
          creature.translated = false;
        }
      });
      fishes.sort((a, b) {
        if (a != null &&
            a.translation != null &&
            b != null &&
            b.translation != null) {
          return a.translation.compareTo(b.translation);
        }
        return -1;
      });
    });
    debugPrint('creatures and translation initialized');
  }

  Future<void> setUserData() async {
    await dataService.loadUserData().then((loadedUserData) {
      this.userData = loadedUserData;
      debugPrint('saved userdata initialized');
    });
  }

  Future<void> changeThern(bool value) async {
    userData.north = value;
    await dataService.saveUserData(userData);
  }

  Future<void> changeSortSpawn(bool value) async {
    userData.sortSpawn = !value;
    await dataService.saveUserData(userData);
  }

  Future<void> changeAnimatedBackground(bool value) async {
    userData.animatedBackground = value;
    await dataService.saveUserData(userData);
    notifyListeners();
  }

  Future<void> saveUserData() async {
    await dataService.saveUserData(userData);
  }

  Future<void> setCatchedData() async {
    await dataService.loadCatches().then(
      (value) {
        myFishes = value;
        if (myFishes.catched.isEmpty) {
          debugPrint('empty catched data');
          fishes.forEach((element) {
            myFishes.catched[element.uniqueEntryId] = false;
          });
        }
        debugPrint('saved catched data initialized');
      },
      onError: (error) => debugPrint(error.toString()),
    );
  }

  Future<void> deleteCatches() async {
    myFishes = await dataService.deleteCatches();
    notifyListeners();
  }

  /*
  Future<void> saveCatched(Catches catches) async {
    await dataService.saveCatched(catches);
    notifyListeners();
  }*/

  int getAllCatchedFishes() {
    int result = myFishes.getAllCatchedFished();
    return result;
  }

  int getAllCatchedFishesMoney() {
    int result = 0;
    myFishes.dailyCatches.forEach((daily) {
      daily.catches.forEach((key, value) {
        result += getFish(key).sell * value;
      });
    });
    return result;
  }

  int getTodaysCatchedFishes() {
    int result = 0;
    String key = getDateKeyString(DateTime.now());
    if (myFishes != null) {
      myFishes.dailyCatches
          .where((element) => element.key == key)
          .toList()
          .forEach((daily) {
        daily.catches.forEach((key, value) {
          result += value;
        });
      });
    }
    return result;
  }

  int getTodaysCatchedFish(String uniqueEntryId) {
    int result = 0;
    String key = getDateKeyString(DateTime.now());
    if (myFishes != null) {
      myFishes.dailyCatches
          .where((element) => element.key == key)
          .toList()
          .forEach((daily) {
        result += daily.catches[uniqueEntryId];
      });
    }
    return result;
  }

  int getAllCatchedFish(String uniqueEntryId) {
    int result = 0;
    if (myFishes != null) {
      result = myFishes.getAllCatchesForFish(uniqueEntryId);
    }
    return result;
  }

  int getTodaysCatchedFishesMoney() {
    int result = 0;
    String key = getDateKeyString(DateTime.now());
    if (myFishes != null) {
      myFishes.dailyCatches
          .where((element) => element.key == key)
          .toList()
          .forEach((daily) {
        daily.catches.forEach((key, value) {
          result += getFish(key).sell * value;
        });
      });
    }
    return result;
  }

  Future<void> setCatchForToday(String uniqueEntryId, int value) async {
    DateTime now = DateTime.now();
    DailyCatches dayCatches = getDaysCatched(now);
    dayCatches.catches[uniqueEntryId] = value;
    await dataService.saveCatched(myFishes);
    notifyListeners();
  }

  Future<void> saveCatched() async {
    await dataService.saveCatched(myFishes);
    notifyListeners();
  }

  DailyCatches getDaysCatched(DateTime time) {
    String key = getDateKeyString(time);
    String formatted = getFormattedDate(time);
    List found =
        myFishes.dailyCatches.where((element) => element.key == key).toList();
    DailyCatches dayCatches;
    if (found == null || found.isEmpty) {
      debugPrint('no saved data for today');
      Map all = Map();
      fishes.forEach((element) => all[element.uniqueEntryId] = 0);
      dayCatches = new DailyCatches(key, formatted, all);
      myFishes.dailyCatches.add(dayCatches);
    } else {
      dayCatches = found[0];
    }
    return dayCatches;
  }

  String getDateKeyString(DateTime time) {
    String result = "";
    result += time.year.toString() +
        "_" +
        time.month.toString() +
        "_" +
        time.day.toString();
    return result;
  }

  String getFormattedDate(DateTime time) {
    String result = time.toIso8601String();
    return result;
  }

  String getFishName(String uniqueEntryId) {
    return getFish(uniqueEntryId).translation;
  }

  Creature getFish(String uniqueEntryId) {
    return fishes
        .firstWhere((element) => element.uniqueEntryId == uniqueEntryId);
  }

  List<Creature> getFishes() {
    List<Creature> result = fishes;
    return result;
  }

  List<Creature> getCurrentFishes(int month, int hour) {
    List<Creature> result = [];
    //debugPrint('get fishes for month ${month.toString()}, hour: ${hour.toString()}');
    fishes.forEach((element) {
      if (element.canBeCatchedInMonthAtTime(userData.getNorth(), month, hour)) {
        result.add(element);
      }
    });
    return result;
  }

  int getDifferentCatchedFishes() {
    int result = 0;
    fishes.forEach((fish) {
      if (myFishes.catched[fish.uniqueEntryId] == true) {
        result++;
      }
    });
    return result;
  }

  Creature getFishOfTheDay() {
    int seed = DateTime.now().day+(DateTime.now().month*31)+DateTime.now().year;
    Random r = Random(seed);
    if (fishes.length > 0) {
      int pos = r.nextInt(fishes.length);
      return fishes[pos];
    }
    return null;
  }
}
