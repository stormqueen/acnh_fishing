import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/material.dart';

import '../locator.dart';


class FishingProvider with ChangeNotifier {
  bool sortByAbc = false;

  final Catches catches;

  List<Creature> allCreatures = [];
  List<Creature> creatures = [];

  Map<String, int> catched = Map();
  Map<String, int> catchedThisSession = Map();


  int index = 0;//0 = sea, 1 = not sea, 2 = all
  int seaSize = 0;
  int noSeaSize = 0;

  FishingProvider(this.catches, this.creatures, todaysCatches, this.sortByAbc) {
    allCreatures = creatures;
    todaysCatches.catches.forEach((key, value) => catched[key] = value);
    todaysCatches.catches.forEach((key, value) => catchedThisSession[key] = 0);
    seaSize = allCreatures.where((element) => element.where == Where.Sea || element.where == Where.Pier).toList().length;
    noSeaSize = allCreatures.where((element) => element.where != Where.Sea && element.where != Where.Pier).toList().length;
    filter();
  }

  filter() {
    if (index == 0) {
      creatures = allCreatures.where((element) => element.where == Where.Sea || element.where == Where.Pier).toList();
    } else if (index == 1) {
      creatures = allCreatures.where((element) => element.where != Where.Sea && element.where != Where.Pier).toList();
    } else {
      creatures = allCreatures;
    }
    sortCreatures();
    notifyListeners();
  }

  int getCatchCounter(String uniqueEntryId) {
    return catched[uniqueEntryId] ?? 0;
  }

  void setSortByAbc(bool value) {
    sortByAbc = value;
    sortCreatures();
    notifyListeners();
  }

  void sortCreatures() {
    if (sortByAbc) {
      debugPrint('sort alpha');
      creatures.sort((a, b) {
        return a.translation.compareTo(b.translation);
      });
    } else {
      creatures.sort((a, b) {
        double rateA = a.getSpawnRate();
        double rateB = b.getSpawnRate();
        if (rateA == rateB) {
          return a.translation.compareTo(b.translation);
        }
        return rateB.compareTo(rateA);
      });
    }

  }

  int getAllCatchedFished() {
    int result = 0;
    catched.values.forEach((element) => result += element);
    return result;
  }

  int getAllCatchedFishedThisSession() {
    int result = 0;
    catchedThisSession.values.forEach((element) => result += element);
    return result;
  }

  addCatched(String uniqueEntryId) async {
    int newValue = getCatchCounter(uniqueEntryId) + 1;
    catched[uniqueEntryId] = newValue;
    catchedThisSession[uniqueEntryId] += 1;
    locator<CreatureService>().setCatchForToday(uniqueEntryId, newValue);
    notifyListeners();
  }

  removeCatched(String uniqueEntryId) {
    int result = getCatchCounter(uniqueEntryId) > 0 ? getCatchCounter(uniqueEntryId) - 1 : 0;
    catched[uniqueEntryId] = result;
    if (catchedThisSession[uniqueEntryId] > 0) {
      catchedThisSession[uniqueEntryId] -= 1;
    }
    locator<CreatureService>().setCatchForToday(uniqueEntryId, result);
    notifyListeners();
  }

  bool neverFound(String uniqueEntryId) {
    bool result = false;
    if (catches.catched.containsKey(uniqueEntryId)) {
      result = !catches.catched[uniqueEntryId];
    }
    return result;
  }
}
