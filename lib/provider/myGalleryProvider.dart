import 'package:acnhfishingbuddy/helper/filterHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/material.dart';

import '../locator.dart';
import 'filter.dart';

enum Sort { alphabetic, spawn }


class MyGalleryProvider with ChangeNotifier {

  final Catches catches;
  final List<Creature> allCreatures;
  final bool north;

  bool sortByAbc = false;

  bool editMode = false;
  bool showTip = false;

  bool viewModeList = false;

  List<Creature> creatures = [];
  List<FishFilterValue> selectedFilter = [];


  MyGalleryProvider(this.allCreatures, this.catches, this.north, this.sortByAbc) {
    debugPrint('create MyGalleryProvider');
    allCreatures.forEach((element) {creatures.add(element);});
    sortCreatures();
  }

  void setSortByAbc(bool value) {
    sortByAbc = value;
    sortCreatures();
    notifyListeners();
  }

  void sortCreatures() {
    if (sortByAbc) {
      creatures.sort((a, b) => a.translation.compareTo(b.translation));
    } else {
      creatures.sort((a, b) => a.num.compareTo(b.num));
    }
  }

  List<Creature> getFilteredCreatures() {
    List<Creature> result = FilterHelper.getFilteredCreatures(creatures, selectedFilter, catches, north);
    return result;
  }

  int getDifferentCatchedFishes() {
    int result = 0;
    getFilteredCreatures().forEach((fish) {
      if (catches.catched[fish.uniqueEntryId] == true) {
        result++;
      }
    });
    return result;
  }



  bool isCatchted(String uniqueEntryId) {
    bool result = catches.catched[uniqueEntryId];
    //debugPrint('catched $uniqueEntryId: $result');
    return result;
  }

  swapEditMode() {
    editMode = !editMode;
    showTip = editMode;
    //debugPrint('editmode: $editMode');
    if (editMode) {
      selectedFilter = [];
      sortByAbc = false;
    } else {
      sortByAbc = true;
    }
    sortCreatures();
    notifyListeners();
  }

  swapListMode() {
    viewModeList = !viewModeList;
    debugPrint('viewModeList: viewModeList');
    notifyListeners();
  }

  cancelShowTip() {
    showTip = false;
    notifyListeners();
  }

  swapCatched(String uniqueEntryId) {
    debugPrint('swap');
    catches.catched[uniqueEntryId] = !catches.catched[uniqueEntryId];
    locator<CreatureService>().saveCatched();
    notifyListeners();
  }

  bool isSelected(FishFilterValue value) {
    return selectedFilter.contains(value);
  }

  setFilter(FishFilterValue value) {
    if (selectedFilter.contains(value)) {
      debugPrint('value removed');
      selectedFilter.remove(value);
    } else {
      debugPrint('value added');
      selectedFilter.add(value);
    }
    notifyListeners();
  }

  setTodoFilter() {
    selectedFilter.clear();
    selectedFilter.add(FishFilterValue.NowAvailable);
    selectedFilter.add(FishFilterValue.NotExisting);
    notifyListeners();
  }

  updateFilter() {
    notifyListeners();
  }

}