import 'package:acnhfishingbuddy/helper/filterHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:flutter/material.dart';

import 'filter.dart';

class TodoProvider with ChangeNotifier {

  final Catches catches;
  final List<Creature> allCreatures;
  final bool north;

  bool sortByAbc = false;
  bool allCatchedThisMonth = false;
  int toCatchThisMonth = 0;
  int nowCatchable = 0;
  int nowNotCatchable = 0;

  List<FishFilterValue> selectedFilter = [];

  TodoProvider(this.catches, this.allCreatures, this.north, this.sortByAbc) {
    this.sortByAbc = true;
    selectedFilter.add(FishFilterValue.CurrentMonth);
    selectedFilter.add(FishFilterValue.NotExisting);
    selectedFilter.add(FishFilterValue.NowAvailable);
    //selectedFilter.add(FishFilterValue.NowNotAvailable);
    List<Creature> all = FilterHelper.getFilteredCreatures(allCreatures, [FishFilterValue.CurrentMonth, FishFilterValue.NotExisting], catches, north);
    allCatchedThisMonth = all.length == 0;
    toCatchThisMonth = all.length;
    //selectedFilter.remove(FishFilterValue.NowNotAvailable);
    nowCatchable = FilterHelper.getFilteredCreatures(allCreatures, [FishFilterValue.CurrentMonth, FishFilterValue.NotExisting, FishFilterValue.NowAvailable], catches, north).length;
    nowNotCatchable = toCatchThisMonth - nowCatchable;
  }

  bool isSelected(FishFilterValue value) {
    return selectedFilter.contains(value);
  }

  void switchNow(bool value) {
    if (selectedFilter.contains(FishFilterValue.NowAvailable)) {
      selectedFilter.remove(FishFilterValue.NowAvailable);
    } else {
      selectedFilter.add(FishFilterValue.NowAvailable);
    }
    notifyListeners();
  }

  void switchNotNow(bool value) {
    if (selectedFilter.contains(FishFilterValue.NowNotAvailable)) {
      selectedFilter.remove(FishFilterValue.NowNotAvailable);
    } else {
      selectedFilter.add(FishFilterValue.NowNotAvailable);
    }
    notifyListeners();
  }

  List<Creature> getFilteredCreatures() {
    List<Creature> result = FilterHelper.getFilteredCreatures(allCreatures, selectedFilter, catches, north);
    sortCreatures(result);
    //debugPrint('size of filtered creatures: ${result.length}');
    return result;
  }

  bool isLastMonth(Creature creature) {
    int month = DateTime.now().month;
    int nextMonth = month + 1;
    if (nextMonth == 13) {
      nextMonth = 1;
    }
    return !creature.getActiveMonth(north).contains(nextMonth);
  }

  void sortCreatures(List<Creature> creatures) {
    if (sortByAbc) {
      creatures.sort((a, b) {
        return a.translation.compareTo(b.translation);
      });
    } else {
      creatures.sort((a, b) {
        double rateA = a.getSpawnRate();
        double rateB = b.getSpawnRate();
        if (rateA == rateB) {
          return a.translation.compareTo(b.translation);
        }
        return rateB.compareTo(rateA);
      });
    }
  }
}