import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/provider/filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TranslationHelper {
  TranslationHelper();

  static String getShadowTranslation(BuildContext context, ShadowSize size) {
    String key = size.toString().replaceAll('.', '');
    String shadowSize = FlutterI18n.translate(context, key);
    return shadowSize;
  }

  static String getWhereTranslation(BuildContext context, Where where) {
    String key = where.toString().replaceAll('.', '');
    String whereT = FlutterI18n.translate(context, key);
    return whereT;
  }
  static String getWhereShortTranslation(BuildContext context, Where where) {
    String key = where.toString().replaceAll('.', '')+'_s';
    String whereT = FlutterI18n.translate(context, key);
    return whereT;
  }

  static String getFishFilterTranslation(BuildContext context, FishFilterValue value) {
    String key = value.toString().replaceAll('.', '');
    String whereT = FlutterI18n.translate(context, key);
    debugPrint("get translation for key $key: $whereT");
    return whereT;
  }
}