import 'package:flutter/material.dart';

class FishingThemeData {

  static const double bigWidth = 800;

  static const double smallWidth = 600;

  /// return true if width is bigger than 800
  static bool isBigScreen(BuildContext context) {
    return MediaQuery.of(context).size.height > bigWidth;
  }

  static bool isSmallScreen(BuildContext context) {
    return MediaQuery.of(context).size.height < smallWidth;
  }

  static double getInfoTextSize(BuildContext context) {
    if(isSmallScreen(context)) {
      return 0.9;
    } else if (isBigScreen(context)) {
      return 1.6;
    } else {
      return 1.0;
    }
  }

  static double getBorderRadius() {
    return 15;
  }

  static double getCardPaddingFromTop(BuildContext context) {
    return isBigScreen(context) ? 20 : 10;
  }

  /* colors */

  static Color getFirstText() {
    return Color(0xff212121);
  }

  static Color getSecondaryText() {
    return Color(0xff757575);
  }

  static Color getDividerColor() {
    return Color(0xffbdbdbd);
  }

  static Color getFirsColor() {
    return Colors.cyan;
  }

  static Color getLinkColor() {
    return Colors.cyan[800];
  }
  static Color getFirstDark() {
    return Color(0xff0097a7);
  }

  static Color getFirstLight() {
    return Color(0xffB2EBF2);
  }

  static Color getSecondColor() {
    return Color(0xFFFFF8F8);//Colors.white;
  }

  static Color getCardBackgroundColorNeverFound() {
    return Color(0xFFFFcdcd);

    //50: Color(0xFFFFEBEE),
    //100: Color(0xFFFFCDD2),
  }

  static Color getCardBackgroundColorNotFoundToday() {
    return Colors.cyan[100];
  }

  static Color getCardBackgroundColor() {
    return Colors.white;
  }

  static Color getSplashColor() {
    return Colors.cyan[200];
  }

  static Color getSpecialIconColor() {
    return Colors.yellow[300];
  }

  static Color getDrawerHeaderBackgroundColor() {
    return getFirsColor();//Colors.cyan[200];
  }

  static Color getScaffoldBackground() {
    return Colors.blueGrey.shade600;
    //return Colors.blueGrey[100];
  }

  static Color getUnselectedBottomNavigationBar() {
    return Colors.grey[700];
  }

  static Color getActiveMonthColor() {
    return Colors.cyan;//Color(0xff0097a7); //Colors.green;
  }

  static Color getInactiveMonthColor() {
    return Colors.grey;
  }

  static Color getTipColor() {
    return Colors.black54;
  }
}