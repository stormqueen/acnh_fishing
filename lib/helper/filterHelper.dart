import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:acnhfishingbuddy/provider/filter.dart';

class FilterHelper {

  static List<Creature> getFilteredCreatures(List<Creature> creatures, List<FishFilterValue> selectedFilter, Catches c, bool north) {
    List<Creature> result = [];

    bool filterActive = false;
    if (selectedFilter.contains(FishFilterValue.Sea)) {
      result.addAll(filterWhere(creatures, Where.Sea));
      filterActive = true;
    }
    if (selectedFilter.contains(FishFilterValue.River)) {
      result.addAll(filterWhere(creatures, Where.River));
      filterActive = true;
    }
    if (selectedFilter.contains(FishFilterValue.Pier)) {
      result.addAll(filterWhere(creatures, Where.Pier));
      filterActive = true;
    }
    if (selectedFilter.contains(FishFilterValue.Pond)) {
      result.addAll(filterWhere(creatures, Where.Pond));
      filterActive = true;
    }
    if (!filterActive) {
      creatures.forEach((element) {
        result.add(element);
      });
    }

    if (selectedFilter.contains(FishFilterValue.NotExisting) || selectedFilter.contains(FishFilterValue.Existing)) {
      List<Creature> tmp = [];
      if (selectedFilter.contains(FishFilterValue.NotExisting)) {
        tmp.addAll(filterExisting(result, c, true));
      }
      if (selectedFilter.contains(FishFilterValue.Existing)) {
        tmp.addAll(filterExisting(result, c, false));
      }
      result = tmp;
    }

    if (selectedFilter.contains(FishFilterValue.CurrentMonth)) {
      result = filterMonth(result, north);
    }

    if (selectedFilter.contains(FishFilterValue.NowAvailable) || selectedFilter.contains(FishFilterValue.NowNotAvailable)) {
      List<Creature> tmp = [];
      if (selectedFilter.contains(FishFilterValue.NowAvailable)) {
        tmp.addAll(filterAvailable(result, north));
      }
      if (selectedFilter.contains(FishFilterValue.NowNotAvailable)) {
        tmp.addAll(filterNotAvailable(result, north));
      }
      result = tmp;
    }

    return result;
  }

  static List<Creature> filterExisting(List<Creature> creatures, Catches c, bool not) {
    List<Creature> result = [];
    creatures.forEach((element) {
      if (not && !c.isCatched(element.uniqueEntryId)) {
         result.add(element);
      } else if (!not && c.isCatched(element.uniqueEntryId)) {
        result.add(element);
      }

    });
    return result;
  }

  static List<Creature> filterWhere(List<Creature> creatures, Where where) {
    List<Creature> result = [];
    creatures.forEach((element) {
      if (where == Where.River) {
        if (element.where == Where.River ||
            element.where == Where.RiverMouth ||
            element.where == Where.RiverTop) {
          result.add(element);
        }
      } else {
        if (element.where == where) {
          result.add(element);
        }
      }
    });
    return result;
  }

  static List<Creature> filterMonth(List<Creature> creatures, bool north) {
    int month = DateTime.now().month;
    List<Creature> result = [];
    creatures.forEach((creature) {
      if (creature.canBeCatchedInMonth(north, month)) {
        result.add(creature);
      }
    });
    return result;
  }

  static List<Creature> filterAvailable(List<Creature> creatures, bool north) {
    int month = DateTime.now().month;
    int hour = DateTime.now().hour;
    List<Creature> result = [];
    creatures.forEach((creature) {
      if (creature.canBeCatchedInMonthAtTime(north, month, hour)) {
        result.add(creature);
      }
    });
    return result;
  }

  static List<Creature> filterNotAvailable(List<Creature> creatures, bool north) {
    int month = DateTime.now().month;
    int hour = DateTime.now().hour;
    List<Creature> result = [];
    creatures.forEach((creature) {
      if (creature.canBeCatchedInMonth(north, month)) {
        if (!creature.canBeCatchedInMonthAtTime(north, month, hour)) {
          result.add(creature);
        }
      }
    });
    return result;
  }
}
