import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/model/savedData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../locator.dart';

class TopFishesCardWidget extends StatelessWidget {
  final DailyCatches dailyCatches;

  const TopFishesCardWidget({Key key, this.dailyCatches}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<ListEntry> list = [];
    dailyCatches.catches.values.toList()..sort();
    dailyCatches.catches.forEach((key, value) {
      list.add(ListEntry(key, value));
    });
    list.sort((a, b) => b.value.compareTo(a.value));

    int c1 = locator<CreatureService>().getTodaysCatchedFish(list[0].key);
    int c2 = locator<CreatureService>().getTodaysCatchedFish(list[1].key);
    int c3 = locator<CreatureService>().getTodaysCatchedFish(list[2].key);

    Creature creature1 = locator<CreatureService>().getFish(list[0].key);
    Creature creature2 = locator<CreatureService>().getFish(list[1].key);
    Creature creature3 = locator<CreatureService>().getFish(list[2].key);

    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(FishingThemeData.getBorderRadius()),
            topRight: Radius.circular(FishingThemeData.getBorderRadius()),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: FishingThemeData.getFirsColor(),
                  borderRadius: BorderRadius.only(
                    topLeft:
                        Radius.circular(FishingThemeData.getBorderRadius()),
                    topRight:
                        Radius.circular(FishingThemeData.getBorderRadius()),
                  )),
              child: ListTile(
                //leading: Icon(Icons.format_list_numbered, color: FishingThemeData.getSecondColor(),),
                leading:
                    FaIcon(FontAwesomeIcons.trophy, color: FishingThemeData.getSpecialIconColor()),
                title: Text(
                  FlutterI18n.translate(context, 'topOverviewHeader'),
                  textScaleFactor: 1.5,
                  style: TextStyle(
                    color: FishingThemeData.getSecondColor(),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TopFishesEntry(1, c1, creature1),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TopFishesEntry(2, c2, creature2),
                  ),
                  TopFishesEntry(3, c3, creature3),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TopFishesEntry extends StatelessWidget {
  final int place;
  final int catches;
  final Creature creature;

  const TopFishesEntry(this.place, this.catches, this.creature);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            flex: 1, child: Text('${place.toString()}.', textScaleFactor: 1.2)),
        Expanded(
            flex: 6, child: catches > 0 ? Text(creature.translation, textScaleFactor: 1.2) : Text('...'),),
        Expanded(
          flex: 3,
          child: catches > 0 ? InkWell(
            onTap: () => Navigator.pushNamed(
                context, 'detail/${creature.uniqueEntryId}'),
            child: Image.network(creature.critterpediaImage,height: 60,),
          ) : Container(height: 40,)
        ),
        Expanded(
          flex: 2,
          child: Align(
            alignment: Alignment.centerRight,
            child: catches > 0 ? Text(
              catches.toString(),
              textScaleFactor: 1.2,
              style: TextStyle(fontWeight: FontWeight.bold),
            ) : Container(height: 40,),
          ),
        )
      ],
    );
  }
}

class ListEntry {
  final String key;
  final int value;

  ListEntry(this.key, this.value);
}
