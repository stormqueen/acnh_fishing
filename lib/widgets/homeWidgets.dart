import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../locator.dart';

class SmallUserTitleRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool nameIsSet = locator<CreatureService>().userData.name != null && locator<CreatureService>().userData.name.isNotEmpty;
    bool islandIsSet = locator<CreatureService>().userData.islandName != null && locator<CreatureService>().userData.islandName.isNotEmpty;
    String text = nameIsSet ? locator<CreatureService>().userData.name+' - ' : '';
    text += islandIsSet ? locator<CreatureService>().userData.islandName : '';
    if (text.isEmpty) {
      text = FlutterI18n.translate(context, "enterSettings");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(text, textScaleFactor: FishingThemeData.isBigScreen(context) ? 1.5 : 1,),
        (!nameIsSet && !islandIsSet) ? IconButton(
          icon: Icon(Icons.settings),
          onPressed: () => Navigator.pushNamed(context, 'settings'),
          color: FishingThemeData.getFirsColor(),
          iconSize: 24,
        ) : Container()
      ],
    );
  }
}


class SmallInfoCard extends StatelessWidget {
  Future<void> _shareText(BuildContext context) async {
    String catchedToday = locator<CreatureService>().getTodaysCatchedFishes().toString();
    String moneyToday = locator<CreatureService>().getTodaysCatchedFishesMoney().toString();
    String allCatched = locator<CreatureService>().getAllCatchedFishes().toString();
    String allMoney = locator<CreatureService>().getAllCatchedFishesMoney().toString();

    String title = FlutterI18n.translate(context, "statisticToSend");
    String today = FlutterI18n.translate(context, 'todaysStatisticData', translationParams: {'fishes': catchedToday, 'money': moneyToday});
    String all = FlutterI18n.translate(context, 'allStatisticData', translationParams: {'fishes': allCatched, 'money': allMoney});

    try {
      Share.text(title, today+'\n\n'+all, 'text/plain');
    } catch (e) {
      print('error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    double paddingTop = FishingThemeData.isBigScreen(context) ? 12 : 8;
    String catchedToday = locator<CreatureService>().getTodaysCatchedFishes().toString();
    String moneyToday = locator<CreatureService>().getTodaysCatchedFishesMoney().toString();
    String allCatched = locator<CreatureService>().getAllCatchedFishes().toString();
    String allMoney = locator<CreatureService>().getAllCatchedFishesMoney().toString();

    return ListTile(
      isThreeLine: true,
      title: Padding(
        padding: EdgeInsets.only(top: paddingTop),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SmallUserTitleRow(),
            IconButton(
              icon: Icon(Icons.share, color: FishingThemeData.getFirsColor(),),
              onPressed: () async => await _shareText(context),
            ),
          ],
        ),
      ),
      subtitle: Padding(
        padding: EdgeInsets.only(top: paddingTop, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FishOverviewLineWidget(
              title: FlutterI18n.translate(context, "today"),
              count: catchedToday,
              money: moneyToday,
            ),
            FishOverviewLineWidget(
              title: FlutterI18n.translate(context, "sum"),
              count: allCatched,
              money: allMoney,
            ),
            /*Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Icon(Icons.share),
                  onPressed: () async => await _shareText(context),
                )
              ],
            ),*/
          ],
        ),
      ),
    );
  }
}

class BigInfoCard extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    bool nameIsSet = locator<CreatureService>().userData.name != null && locator<CreatureService>().userData.name.isNotEmpty;
    bool islandIsSet = locator<CreatureService>().userData.islandName != null && locator<CreatureService>().userData.islandName.isNotEmpty;
    String text = nameIsSet ? locator<CreatureService>().userData.name+' - ' : '';
    text += islandIsSet ? locator<CreatureService>().userData.islandName : '';
    if (text.isEmpty) {
      text = FlutterI18n.translate(context, "enterSettings");
    }

    return ListTile(
      isThreeLine: true,
      leading: FaIcon(
        FontAwesomeIcons.tree,
        color: Colors.cyan,
        size: 30,
      ),
      title: Text(text),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            FixedFishOverviewLineWidget(
              title: FlutterI18n.translate(context, "today"),
              count: locator<CreatureService>()
                  .getTodaysCatchedFishes()
                  .toString(),
              money: locator<CreatureService>()
                  .getTodaysCatchedFishesMoney()
                  .toString(),
            ),
            FixedFishOverviewLineWidget(
              title: FlutterI18n.translate(context, "sum"),
              count:
              locator<CreatureService>().getAllCatchedFishes().toString(),
              money: locator<CreatureService>()
                  .getAllCatchedFishesMoney()
                  .toString(),
            ),

          ],
        ),
      ),
      /*trailing: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            flex: 6,
            child: FishOverviewLineWidget(
              title: FlutterI18n.translate(context, "today"),
              count: locator<CreatureService>()
                  .getTodaysCatchedFishes()
                  .toString(),
            ),
          ),
          Expanded(
            flex: 6,
            child: FishOverviewLineWidget(
              title: FlutterI18n.translate(context, "sum"),
              count: locator<CreatureService>()
                  .getAllCatchedFishes()
                  .toString(),
            ),
          ),
        ],
      ),*/
    );
  }
}


class FishOverviewLineWidget extends StatelessWidget {
  final String title;
  final String count;
  final String money;

  const FishOverviewLineWidget(
      {Key key,
        @required this.title,
        @required this.count,
        @required this.money})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
              flex: 3,
              child: Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          Expanded(
            flex: 4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 5),
                    child: Text(count),
                  ),
                ),
                FaIcon(
                  FontAwesomeIcons.fish,
                  color: FishingThemeData.getFirsColor(),
                  size: 18,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 5),
                    child: Text(money),
                  ),
                ),
                FaIcon(
                  FontAwesomeIcons.solidStar,
                  color: FishingThemeData.getFirsColor(),
                  size: 18,
                ),
              ],
            ),
          ),
         /* Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
               IconButton(
                 icon: Icon(Icons.share),
                 onPressed: () async => await _shareText(),
               )
              ],
            ),
          ),*/
        ],
    );
  }/*
  Future<void> _shareText() async {
    try {
      Share.text(shareTextTitle, shareText, 'text/plain');
    } catch (e) {
      print('error: $e');
    }
  }*/
}

class FixedFishOverviewLineWidget extends StatelessWidget {
  final String title;
  final String count;
  final String money;

  const FixedFishOverviewLineWidget(
      {Key key,
        @required this.title,
        @required this.count,
        @required this.money})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 80,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 5),
                    child: Text(count),
                  ),
                ),
                FaIcon(
                  FontAwesomeIcons.fish,
                  color: FishingThemeData.getFirsColor(),
                  size: 18,
                ),
              ],
            ),
          ),
          SizedBox(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 5),
                    child: Text(money),
                  ),
                ),
                FaIcon(
                  FontAwesomeIcons.solidStar,
                  color: FishingThemeData.getFirsColor(),
                  size: 18,
                ),
              ],
            ),
          ),
        ]);
  }
}