import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:flutter/material.dart';

class InfoCardWidget extends StatelessWidget {
  final List<Widget> children;

  const InfoCardWidget({Key key, this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.95,
      decoration: BoxDecoration(
        color: FishingThemeData.getCardBackgroundColor(),
        borderRadius: BorderRadius.circular(FishingThemeData.getBorderRadius()),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        ),
      ),
    );
  }
}