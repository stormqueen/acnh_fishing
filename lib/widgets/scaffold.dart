import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/provider/filter.dart';
import 'package:acnhfishingbuddy/provider/myGalleryProvider.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/material.dart';
import 'package:acnhfishingbuddy/provider/fishingProvider.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../locator.dart';

class MyGalleryScaffold extends StatelessWidget {
  final Widget child;
  final Widget floatingActionButton;
  final String title;

  const MyGalleryScaffold(
      {Key key, this.child, this.title, this.floatingActionButton})
      : super(key: key);

  void _selectFilter(BuildContext context, FishFilterValue filter) {
    context.read<MyGalleryProvider>().setFilter(filter);
  }

  void _updateFilter(BuildContext context) {
    context.read<MyGalleryProvider>().updateFilter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(title),
          ],
        ),
        actions: [
          /*Row(
            children: [
              Icon(Icons.sort_by_alpha),
              Switch(
                inactiveThumbColor: Colors.grey[400],
                activeTrackColor: Colors.grey[200],
                activeColor:  FishingThemeData.getSecondColor(),
                value: context.watch<MyGalleryProvider>().sortByAbc,
                onChanged: (value) => context.read<MyGalleryProvider>().setSortByAbc(value),
              )
            ],
          ),*/
          IconButton(
            icon: context.watch<MyGalleryProvider>().viewModeList
                ? Icon(Icons.apps)
                : Icon(Icons.format_list_bulleted),
            onPressed: () => context.read<MyGalleryProvider>().swapListMode(),
            tooltip: context.watch<MyGalleryProvider>().viewModeList
                ? FlutterI18n.translate(context, 'toGalleryMode')
                : FlutterI18n.translate(context, 'toListMode'),
          ),
          /*IconButton(
            icon: FaIcon(FontAwesomeIcons.clipboardList),
            onPressed: () => context.read<MyGalleryProvider>().setTodoFilter(),
          ),*/
          PopupMenuButton<FishFilterValue>(
              onSelected: (value) => _selectFilter(context, value),
              onCanceled: () => _updateFilter(context),
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<FishFilterValue>>[
                    createItem(context, FishFilterValue.Sea),
                    createItem(context, FishFilterValue.Pier),
                    createItem(context, FishFilterValue.River),
                    createItem(context, FishFilterValue.Pond),
                    PopupMenuDivider(
                      height: 20,
                    ),
                    createItem(context, FishFilterValue.NotExisting),
                    createItem(context, FishFilterValue.Existing),
                    //createItem(context, FishFilterValue.CurrentMonth),
                    //createItem(context, FishFilterValue.NowAvailable),
                  ])
        ],
      ),
      body: child,
      floatingActionButton: floatingActionButton,
    );
  }

  PopupMenuItem<FishFilterValue> createItem(
      BuildContext context, FishFilterValue filter) {
    return PopupMenuItem<FishFilterValue>(
        value: filter,
        child: ChoiceChip(
          selectedColor: FishingThemeData.getFirsColor(),
          labelStyle: TextStyle(color: Colors.black),
          selected: context.read<MyGalleryProvider>().isSelected(filter),
          label: Row(children: [
            Text(
              TranslationHelper.getFishFilterTranslation(context, filter),
            )
          ]),
        ));
  }
}

class FishScaffold extends StatelessWidget {
  final Widget child;
  final Widget floatingActionButton;
  final String title;

  const FishScaffold(
      {Key key, this.child, this.title, this.floatingActionButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(right: 48.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(title),
            ],
          ),
        ),
      ),
      body: child,
      floatingActionButton: floatingActionButton,
    );
  }
}

class FishingScaffold extends StatelessWidget {
  final Widget child;

  const FishingScaffold({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int allSize = context.watch<FishingProvider>().allCreatures.length;
    int seaSize = context.watch<FishingProvider>().seaSize;
    int noSeaSize = context.watch<FishingProvider>().noSeaSize;
    int index = context.watch<FishingProvider>().index;
    String count = context.watch<FishingProvider>().getAllCatchedFished().toString();
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: FishingThemeData.getFirsColor(),
        selectedIconTheme:
            IconThemeData(color: FishingThemeData.getSecondColor(), size: 22),
        unselectedIconTheme: IconThemeData(
            color: FishingThemeData.getUnselectedBottomNavigationBar(),
            size: 22),
        onTap: (index) {
          context.read<FishingProvider>().index = index;
          context.read<FishingProvider>().filter();
        },
        currentIndex: context.watch<FishingProvider>().index,
        items: [
          BottomNavigationBarItem(
            icon: FaIcon(
              FontAwesomeIcons.ship,
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  FlutterI18n.translate(context, "seaPier"),
                  style: TextStyle(
                      color: index == 0
                          ? FishingThemeData.getSecondColor()
                          : FishingThemeData
                              .getUnselectedBottomNavigationBar()),
                ),
                Text(
                  '(${seaSize.toString()})',
                  style: TextStyle(
                      color: index == 0
                          ? FishingThemeData.getSecondColor()
                          : FishingThemeData
                              .getUnselectedBottomNavigationBar()),
                ),
              ],
            ),
          ),
          BottomNavigationBarItem(
              icon: FaIcon(
                FontAwesomeIcons.water,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    FlutterI18n.translate(context, "noSea"),
                    style: TextStyle(
                        color: index == 1
                            ? FishingThemeData.getSecondColor()
                            : FishingThemeData
                                .getUnselectedBottomNavigationBar()),
                  ),
                  Text(
                    '(${noSeaSize.toString()})',
                    style: TextStyle(
                        color: index == 1
                            ? FishingThemeData.getSecondColor()
                            : FishingThemeData
                                .getUnselectedBottomNavigationBar()),
                  )
                ],
              )),
          BottomNavigationBarItem(
              icon: FaIcon(
                FontAwesomeIcons.fish,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    FlutterI18n.translate(context, "all"),
                    style: TextStyle(
                        color: index == 2
                            ? FishingThemeData.getSecondColor()
                            : FishingThemeData
                                .getUnselectedBottomNavigationBar()),
                  ),
                  Text(
                    '(${allSize.toString()})',
                    style: TextStyle(
                        color: index == 2
                            ? FishingThemeData.getSecondColor()
                            : FishingThemeData
                                .getUnselectedBottomNavigationBar()),
                  )
                ],
              ))
        ],
      ),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                      FlutterI18n.translate(context, 'fishingCountToday', translationParams: {'count': count})
                )),
                FaIcon(
                  FontAwesomeIcons.fish,
                  color: Colors.white,
                  size: 18,
                ),
              ],
            ),
          ],
        ),
        actions: [
          Row(
            children: [
              Icon(Icons.sort_by_alpha),
              Switch(
                inactiveThumbColor: Colors.grey[400],
                activeTrackColor: Colors.grey[200],
                activeColor:  FishingThemeData.getSecondColor(),
                value: context.watch<FishingProvider>().sortByAbc,
                onChanged: (value) => context.read<FishingProvider>().setSortByAbc(value),
              )
            ],
          )
        ],
      ),
      body: child,
    );
  }
}

class MainScaffold extends StatelessWidget {
  final Widget child;

  const MainScaffold({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Creature fishOfTheDay = locator<CreatureService>().getFishOfTheDay();
    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, 'appName')),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(flex: 3, child: Text(FlutterI18n.translate(context, "drawerHeaderText"), style: TextStyle(color: FishingThemeData.getSecondColor()),)),
                    Expanded(flex: 6, child: fishOfTheDay != null ? Image.network(fishOfTheDay.critterpediaImage) : Container()),
                    Expanded(flex: 3, child: fishOfTheDay != null ? Align(alignment: Alignment.bottomCenter, child: Text(fishOfTheDay.translation, style: TextStyle(color: FishingThemeData.getSecondColor()),)) : Container())
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: FishingThemeData.getDrawerHeaderBackgroundColor(),
              ),
            ),
            ListTile(
              title: Text(FlutterI18n.translate(context, "myGallery")),
              leading: FaIcon(FontAwesomeIcons.fish),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'myGallery');
              },
            ),
            ListTile(
              title: Text(FlutterI18n.translate(context, "menu1")),
              leading: Icon(Icons.format_list_bulleted),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'fishList');
              },
            ),
            /*
            ListTile(
              title: Text(FlutterI18n.translate(context, "menu1a")),
              leading: FaIcon(FontAwesomeIcons.book),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'fishList');
              },
            ),*/
            ListTile(
              title: Text(FlutterI18n.translate(context, "menu2")),
              leading: Icon(Icons.settings),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'settings');
              },
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              title: Text(FlutterI18n.translate(context, "menu4")),
              leading: FaIcon(FontAwesomeIcons.questionCircle),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'howto');
              },
            ),
            ListTile(
              title: Text(FlutterI18n.translate(context, "menu3")),
              leading: Icon(Icons.info_outline),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'info');
              },
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              title: Text(FlutterI18n.translate(context, "purchaseHeader")),
              leading: FaIcon(FontAwesomeIcons.moneyBill),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'purchase');
              },
            ),
          ],
        ),
      ),
      body: child,
    );
  }
}
