import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:acnhfishingbuddy/helper/translationHelper.dart';
import 'package:acnhfishingbuddy/locator.dart';
import 'package:acnhfishingbuddy/model/creature.dart';
import 'package:acnhfishingbuddy/services/creatureService.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FishSize extends StatelessWidget {
  final ShadowSize size;

  const FishSize({Key key, @required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double iconSize = 6;
    int count = -1;
    if (ShadowSize.XSmall == size) {
      count = 0;
    } else if (ShadowSize.Small == size) {
      count = 1;
    } else if (ShadowSize.Medium == size) {
      count = 2;
    } else if (ShadowSize.Large == size) {
      count = 3;
    } else if (ShadowSize.XLarge == size) {
      count = 4;
    } else if (ShadowSize.XXLarge == size) {
      count = 5;
    }
    IconData data = FontAwesomeIcons.fish;
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: FishingThemeData.getFirsColor(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: count > 0
                ? FishingThemeData.getFirsColor()
                : FishingThemeData.getScaffoldBackground(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: count > 1
                ? FishingThemeData.getFirsColor()
                : FishingThemeData.getScaffoldBackground(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: count > 2
                ? FishingThemeData.getFirsColor()
                : FishingThemeData.getScaffoldBackground(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: count > 3
                ? FishingThemeData.getFirsColor()
                : FishingThemeData.getScaffoldBackground(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: FaIcon(
            data,
            size: iconSize,
            color: count > 4
                ? FishingThemeData.getFirsColor()
                : FishingThemeData.getScaffoldBackground(),
          ),
        )
      ],
    );
    //return Text(count.toString());
  }
}

class CreatureListTile extends StatelessWidget {
  final Creature creature;
  final List<String> list;

  const CreatureListTile({Key key, this.creature, this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String jsonList = list != null ? json.encode(list) : null;
    int currentIndex =
        list != null ? list.indexOf(creature.uniqueEntryId) : null;
    return Container(
      decoration: BoxDecoration(
        color: FishingThemeData.getCardBackgroundColor(),
        borderRadius: BorderRadius.circular(FishingThemeData.getBorderRadius()),
      ),
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(creature.iconImage, scale: 1.5),
          backgroundColor: Colors.transparent,
        ),
        title: Text(
          creature.translation ?? creature.name,
          textScaleFactor: 2,
        ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(TranslationHelper.getWhereTranslation(context, creature.where),
                textScaleFactor: 1.3),
            Row(
              children: [
                Text(creature.sell.toString(), textScaleFactor: 1.3),
                Icon(
                  Icons.star,
                  color: FishingThemeData.getFirsColor(),
                )
              ],
            )
          ],
        ),
        onTap: () => list != null
            ? Navigator.pushNamed(context,
                'detail/${creature.uniqueEntryId}/$currentIndex/$jsonList')
            : Navigator.pushNamed(context, 'detail/${creature.uniqueEntryId}'),
      ),
    );
  }
}

class CreatureMonthWidget extends StatelessWidget {
  final List<int> month;

  const CreatureMonthWidget({Key key, this.month}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> monthWidgets = [];
    for (int i = 1; i < 13; i++) {
      monthWidgets.add(_createChild(context, i));
    }
    return Row(children: monthWidgets);
  }

  Widget _createChild(BuildContext context, int i) {
    int currentMonth = DateTime.now().month;
    Color color = month.contains(i)
        ? FishingThemeData.getActiveMonthColor()
        : FishingThemeData.getInactiveMonthColor();
    String key = 'm' + i.toString() + 's';
    String translation = FlutterI18n.translate(context, key);
    return Expanded(
      flex: 1,
      child: Container(
        color: color,
        child: Text(
          translation,
          textScaleFactor: 0.8,
          style: TextStyle(
              color: currentMonth == i ? Colors.white : Colors.black87),
        ),
      ),
    );
  }
}

class CreatureTimeWidget extends StatelessWidget {
  final Thern thern;

  const CreatureTimeWidget({Key key, this.thern}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget result = Container();
    if (thern == null) {
      result = Text('-');
    } else {
      List<List<String>> hours = thern.activeHours;
      String text = "";
      int index = 0;
      bool translated = false;
      hours.forEach((hour) {
        String startString = hour[0];
        String endString = hour[1];
        int start = int.parse(startString);
        int end = int.parse(endString);
        if (start == 0 && end == 0) {
          text = FlutterI18n.translate(context, 'allDay');
          translated = true;
        } else {
          if (index > 0) {
            text += ", ";
          }
          text += startString + ' - ' + endString;
        }
        if (!translated) {
          text += FlutterI18n.translate(context, "clock");
        }
        result = Text(text);
        index++;
      });
    }
    return result;
  }
}

class CreatureDetail extends StatelessWidget {
  final String uniqueEntryId;

  const CreatureDetail({Key key, this.uniqueEntryId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool north = locator<CreatureService>().userData.getNorth();
    int month = DateTime.now().month;

    Creature creature = locator<CreatureService>().getFish(uniqueEntryId);
    List<int> creatureActiveMonth = creature.getActiveMonth(north);
    bool isNowActiveMonth = creatureActiveMonth.contains(month);
    bool catched =
        locator<CreatureService>().myFishes.catched[creature.uniqueEntryId];

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Center(
        child: Container(
          decoration: BoxDecoration(
              color: FishingThemeData.getCardBackgroundColor(),
              borderRadius:
              BorderRadius.circular(FishingThemeData.getBorderRadius())),
          width: MediaQuery.of(context).size.width * 0.95,
          child: Column(
            children: [
              Expanded(
                flex: 5,
                child: CreaturePictureWidget(creature: creature),
              ),
              Expanded(
                flex: 1,
                child: CreatureCatchedWidget(uniqueEntryId: uniqueEntryId),
              ),
              Expanded(
                flex: 6,
                child: CreatureInfoWidget(
                    creature: creature,
                    creatureActiveMonth: creatureActiveMonth,
                    north: north,
                    month: month,
                    isNowActiveMonth: isNowActiveMonth,
                    catched: catched ?? false),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CreaturePictureWidget extends StatefulWidget {
  const CreaturePictureWidget({
    Key key,
    @required this.creature,
  }) : super(key: key);

  final Creature creature;

  @override
  _CreaturePictureWidgetState createState() => _CreaturePictureWidgetState();
}

class _CreaturePictureWidgetState extends State<CreaturePictureWidget>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();

    tabController = TabController(
      initialIndex: selectedIndex,
      length: 3,
      vsync: this,
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        TabBar(
          tabs: <Tab>[
            Tab(text: '1'),
            Tab(text: '2'),
            Tab(text: '3'),
          ],
          controller: tabController,
          onTap: (int index) {
            setState(() {
              selectedIndex = index;
              tabController.animateTo(index);
            });
          },
        ),
        Divider(height: 0),
        IndexedStack(
          children: <Widget>[
            TabImageWidget(
              creatureName: widget.creature.translation,
              imagePath: widget.creature.critterpediaImage,
              selectedTab: selectedIndex,
              tabNumber: 0,
            ),
            TabImageWidget(
              creatureName: widget.creature.translation,
              imagePath: widget.creature.iconImage,
              selectedTab: selectedIndex,
              tabNumber: 1,
            ),
            TabImageWidget(
              creatureName: widget.creature.translation,
              imagePath: widget.creature.furnitureImage,
              selectedTab: selectedIndex,
              tabNumber: 2,
            ),


          ],
          index: selectedIndex,
        ),
      ],
    );
  }
}

class TabImageWidget extends StatelessWidget {
  final String creatureName;
  final String imagePath;
  final int tabNumber;
  final int selectedTab;

  const TabImageWidget(
      {Key key, this.creatureName, this.imagePath, this.tabNumber, this.selectedTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Visibility(
        child: GestureDetector(
          onTap: () {
            showDialog(
                context: context,
                builder: (_) => SimpleDialog(
                  title: Text(creatureName),
                  semanticLabel: creatureName,
                  children: [Image.network(
                    imagePath,
                   scale: 0.5,
                  ),],
                )
            );

          },
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(
                imagePath,
                height: MediaQuery.of(context).size.height / 12 * 2.8,
              ),
            ),
          ),
        ),
        maintainState: true,
        visible: selectedTab == tabNumber,
      ),
    );
  }
}

class CreatureCatchedWidget extends StatelessWidget {
  const CreatureCatchedWidget({
    Key key,
    @required this.uniqueEntryId,
  }) : super(key: key);

  final String uniqueEntryId;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: FishingThemeData.getFirsColor(),
      child: Center(
          child: Text(
        FlutterI18n.translate(context, 'fishingCount', translationParams: {
          'count': locator<CreatureService>()
              .getAllCatchedFish(uniqueEntryId)
              ?.toString()
        }),
        textScaleFactor: 1.5,
            style: TextStyle(
              color: FishingThemeData.getSecondColor()
            ),
      )),
    );
  }
}

class CreatureInfoWidget extends StatelessWidget {
  const CreatureInfoWidget({
    Key key,
    @required this.creature,
    @required this.creatureActiveMonth,
    @required this.north,
    @required this.month,
    @required this.isNowActiveMonth,
    @required this.catched,
  }) : super(key: key);

  final Creature creature;
  final List<int> creatureActiveMonth;
  final bool north;
  final int month;
  final bool isNowActiveMonth;
  final bool catched;

  @override
  Widget build(BuildContext context) {
    return Container(
       child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0, top: 12.0),
              child: Column(
                children: [
                  CreatureInfoRow(
                      headline: FlutterI18n.translate(context, 'shadow'),
                      info: TranslationHelper.getShadowTranslation(
                          context, creature.shadowSize)),
                  CreatureInfoRow(
                    headline: '',
                    infoWidget: FishSize(size: creature.shadowSize),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CreatureInfoRow(
                  headline: FlutterI18n.translate(context, 'where'),
                  info: TranslationHelper.getWhereTranslation(
                      context, creature.where)),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CreatureInfoRow(
                  headline: FlutterI18n.translate(context, 'sternis'),
                  info: creature.sell.toString() +
                      ' / ' +
                      creature.specialSell.toString()),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CreatureInfoRow(
                headline: FlutterI18n.translate(context, 'spawnRate'),
                info: creature.spawnRates + ' %',
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CreatureInfoRow(
                headline: FlutterI18n.translate(context, 'month'),
                infoWidget: CreatureMonthWidget(month: creatureActiveMonth),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: CreatureInfoRow(
                headline: FlutterI18n.translate(context, 'fishingTime'),
                infoWidget: CreatureTimeWidget(
                    thern: creature.getCurrentThern(north, month)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: CreatureInfoRow(
                headline: FlutterI18n.translate(context, 'catchable'),
                subheadline: FlutterI18n.translate(context, 'catchableSub'),
                infoWidget: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    isNowActiveMonth
                        ? Icon(
                            Icons.check_circle,
                            color: FishingThemeData.getActiveMonthColor(),
                          )
                        : Icon(
                            Icons.remove_circle,
                            color: FishingThemeData.getInactiveMonthColor(),
                          ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: CreatureInfoRow(
                headline: FlutterI18n.translate(context, 'alreadyCatched'),
                infoWidget: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    catched
                        ? Icon(
                            Icons.check_circle,
                            color: FishingThemeData.getActiveMonthColor(),
                          )
                        : Icon(
                            Icons.remove_circle,
                            color: FishingThemeData.getInactiveMonthColor(),
                          ),
                  ],
                ),
              ),
            ),
            kReleaseMode
                ? Container()
                : CreatureInfoRow(
                    headline: 'name',
                    info: creature.name,
                  ),
            kReleaseMode
                ? Container()
                : CreatureInfoRow(
                    headline: 'unique',
                    info: creature.uniqueEntryId,
                  ),
          ],
        ),
      ),
    );
  }
}

class CreatureInfoRow extends StatelessWidget {
  final String headline;
  final String subheadline;
  final String info;
  final Widget infoWidget;

  const CreatureInfoRow(
      {Key key,
      @required this.headline,
      this.info,
      this.infoWidget,
      this.subheadline})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double scale = FishingThemeData.getInfoTextSize(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          flex: 1,
          child: Container(),
        ),
        Expanded(
          flex: 4,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(headline, textScaleFactor: scale),
              subheadline != null
                  ? Text(subheadline, textScaleFactor: 0.7)
                  : Container()
            ],
          ),
        ),
        Expanded(
          flex: 6,
          child: infoWidget != null
              ? infoWidget
              : Text(info, textScaleFactor: scale),
        ),
        Expanded(
          flex: 1,
          child: Container(),
        )
      ],
    );
  }
}
