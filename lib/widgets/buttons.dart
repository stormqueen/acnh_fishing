import 'package:acnhfishingbuddy/helper/fishingThemeData.dart';
import 'package:flutter/material.dart';

class FishingButton extends StatelessWidget {

  final VoidCallback onPressed;
  final String text;

  const FishingButton({Key key, @required this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width*0.6,
      child: RaisedButton(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text(text, textScaleFactor: FishingThemeData.isSmallScreen(context) ? 1.3 : 1.5,),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
