import 'package:acnhfishingbuddy/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/loaders/decoders/json_decode_strategy.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import 'helper/fishingThemeData.dart';
import 'locator.dart';

Future main() async {
  InAppPurchaseConnection.enablePendingPurchases();
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
    translationLoader: FileTranslationLoader(
        useCountryCode: false, fallbackFile: 'en', basePath: 'assets/i18n', decodeStrategies: [JsonDecodeStrategy()]),
  );
  WidgetsFlutterBinding.ensureInitialized();
  await flutterI18nDelegate.load(null);
  FishingRouter.setupRouter();
  setupLocator();
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom, SystemUiOverlay.top]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  runApp(FishingApp(flutterI18nDelegate));
}

class FishingApp extends StatefulWidget {
  final FlutterI18nDelegate flutterI18nDelegate;

  const FishingApp(this.flutterI18nDelegate);

  @override
  _FishingAppState createState() => _FishingAppState();
}

class _FishingAppState extends State<FishingApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('build FishingApp');
    return _getMaterialDone();
  }

  Widget _getMaterialDone() {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      darkTheme: ThemeData(
        backgroundColor: Colors.black87,
        primaryColor: Colors.cyan,
        primarySwatch: Colors.cyan,
        primaryTextTheme: TextTheme(
          headline6: TextStyle(color: Colors.white),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      theme: ThemeData(
        appBarTheme: Theme.of(context).appBarTheme.copyWith(
            iconTheme: IconThemeData(color: FishingThemeData.getSecondColor())),
        scaffoldBackgroundColor:FishingThemeData.getScaffoldBackground(),
        cardColor: FishingThemeData.getCardBackgroundColor(),
        primaryColor: FishingThemeData.getFirsColor(),
        primarySwatch: FishingThemeData.getFirsColor(),

        buttonTheme: ButtonThemeData(
          buttonColor: FishingThemeData.getFirsColor(),
          disabledColor: Colors.grey,
          textTheme: ButtonTextTheme.accent,
          colorScheme: Theme.of(context).colorScheme.copyWith(secondary: FishingThemeData.getSecondColor()),
          minWidth: 200,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(FishingThemeData.getBorderRadius()),
            //side: BorderSide(color: Colors.grey, width: 2)
          )
        ),

        primaryTextTheme: TextTheme(
            button: TextStyle(color: FishingThemeData.getSecondColor()),

          headline6: TextStyle(color:FishingThemeData.getSecondColor()),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: 'home',
      onGenerateRoute: FishingRouter.router.generator,
      localizationsDelegates: [
        widget.flutterI18nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
    );
  }
}
