import 'package:flutter/material.dart';

class Creature {
  final SourceSheet sourceSheet;
  final int num;
  final String name;
  final String iconImage;
  final String critterpediaImage;
  final String furnitureImage;
  final Size size;
  final int sell;
  final String whereHow;
  final Where where;
  final String shadow;
  final int totalCatchesToUnlock;
  final String spawnRates;
  final LightingType lightingType;
  final String iconFilename;
  final String critterpediaFilename;
  final String furnitureFilename;
  final int internalId;
  final String uniqueEntryId;
  final List<String> colors;
  final int specialSell;
  final ActiveMonths activeMonths;
  final Weather weather;

  final bool hasFin;
  final ShadowSize shadowSize;

  String translation;
  bool translated;

  Creature(
      this.sourceSheet,
      this.num,
      this.name,
      this.iconImage,
      this.critterpediaImage,
      this.furnitureImage,
      this.size,
      this.sell,
      this.whereHow,
      this.shadow,
      this.totalCatchesToUnlock,
      this.spawnRates,
      this.lightingType,
      this.iconFilename,
      this.critterpediaFilename,
      this.furnitureFilename,
      this.internalId,
      this.uniqueEntryId,
      this.colors,
      this.specialSell,
      this.activeMonths,
      this.weather,
      this.where,
      this.hasFin,
      this.shadowSize);

  Creature.fromJson(Map<String, dynamic> jsonString)
      : this.sourceSheet =
            EnumConverter.getSourceSheetFromString(jsonString['sourceSheet']),
        this.num = jsonString['num'],
        this.name = jsonString['name'],
        this.iconImage = jsonString['iconImage'],
        this.critterpediaImage = jsonString['critterpediaImage'],
        this.furnitureImage = jsonString['furnitureImage'],
        this.size = EnumConverter.getSizeFromString(jsonString['size']),
        this.sell = jsonString['sell'],
        this.whereHow = jsonString['whereHow'],
        this.shadow = jsonString['shadow'],
        this.totalCatchesToUnlock = jsonString['totalCatchesToUnlock'],
        this.spawnRates = jsonString['spawnRates'],
        this.lightingType =
            EnumConverter.getLightingTypeFromString(jsonString['lightingType']),
        this.iconFilename = jsonString['iconFilename'],
        this.critterpediaFilename = jsonString['critterpediaFilename'],
        this.furnitureFilename = jsonString['furnitureFilename'],
        this.internalId = jsonString['internalId'],
        this.uniqueEntryId = jsonString['uniqueEntryId'],
        this.colors = jsonString['colors'].cast<String>(),
        this.specialSell = jsonString['specialSell'],
        this.activeMonths = ActiveMonths.fromJson(jsonString['activeMonths']),
        this.weather =
            EnumConverter.getWeatherFromString(jsonString['weather']),
        this.where = jsonString['whereHow'] != null ? EnumConverter.getWhereFromString(jsonString['whereHow']) : Where.Unknown,
        this.hasFin = jsonString['shadow'].toString().contains('Fin'),
        this.shadowSize =
            EnumConverter.getShadowSizeFromString(jsonString['shadow']);

  static getColorsFromJson(String json) {
    List<CreatureColor> result = [];
    List<String> colors = [];
    if (json != null) {
      colors = json as List;
      colors.forEach(
          (element) => result.add(EnumConverter.getColorFromString(element)));
    }

    return result;
  }

  double getSpawnRate() {
    double result = 0;
    List<String> all = spawnRates.split('–');
    int size = all.length;
    int sum = 0;
    all.forEach((element) {
      sum += int.parse(element);
    });
    result = sum / size;
    return result;
  }

  List<int> getActiveMonth(bool north) {
    List<int> result = [];
    List<Thern> thern = activeMonths.northern;
    if (!north) {
      debugPrint('get current thern for south');
      thern = activeMonths.southern;
    }
    thern.forEach((element) => result.add(element.month));
    return result;
  }

  Thern getCurrentThern(bool north, int month) {
    Thern result;
    List<Thern> thern = activeMonths.northern;
    if (!north) {
      debugPrint('get current thern for south');
      thern = activeMonths.southern;
    }
    List tmp = thern.where((element) => element.month == month).toList();
    if (tmp.isNotEmpty) {
      result = tmp.first;
    }
    return result;
  }

  bool canBeCatchedInMonth(bool north, int month) {
    List<int> activeMonth = getActiveMonth(north);
    bool result = activeMonth.contains(month);
    return result;
  }

  bool canBeCatchedInMonthAtTime(bool north, int month, int hour) {
    bool result = false;

    List<Thern> thernList = activeMonths.northern;
    if (!north) {
      debugPrint('get current thern for south');
      thernList = activeMonths.southern;
    }
    thernList.forEach((thern) {
      if (thern.month == month) {
        thern.activeHours.forEach((hours) {
          int start = int.parse(hours[0]);
          int end = int.parse(hours[1]);
          //debugPrint('start: $start, end: $end');
          if (start == 0 && end == 0) {
            //debugPrint('${element.name} all day');
            result = true;
          } else if (start < end && hour >= start && hour < end) {
            //debugPrint('${element.name} between');
            result = true;
          } else if (end < start) {
            if (hour >= start || hour < end) {
              //debugPrint('${element.name} over night');
              result = true;
            }
          } else if (end == start && start == hour) {
            //debugPrint('${element.name} one hour');
            result = true;
          }
        });
      }
    });

    return result;
  }
}

class EnumConverter {
  static ShadowSize getShadowSizeFromString(String enumAsString) {
    if (enumAsString != null) {
      for (ShadowSize element in ShadowSize.values) {
        if (element.toString().split('.').last == enumAsString) {
          return element;
        } else if (enumAsString.contains('XX-Large')) {
          return ShadowSize.XXLarge;
        } else if (enumAsString.contains('X-Large')) {
          return ShadowSize.XLarge;
        } else if (enumAsString.contains('Large') ||
            enumAsString.contains('Long')) {
          return ShadowSize.Large;
        } else if (enumAsString.contains('Medium')) {
          return ShadowSize.Medium;
        } else if (enumAsString.contains('X-Small')) {
          return ShadowSize.XSmall;
        } else if (enumAsString.contains('Small')) {
          return ShadowSize.Small;
        }
      }
    }
    return ShadowSize.Unknown;
  }

  static Where getWhereFromString(String enumAsString) {
    // Sea, River, RiverMouth, RiverTop, Pond, Pier, Unknown
    for (Where element in Where.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      } else if (enumAsString.contains('clifftop')) {
        return Where.RiverTop;
      } else if (enumAsString.contains('mouth')) {
        return Where.RiverMouth;
      } else if (enumAsString.contains('Sea')) {
        return Where.Sea;
      }
    }
    return Where.Unknown;
  }

  static SourceSheet getSourceSheetFromString(String enumAsString) {
    for (SourceSheet element in SourceSheet.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      }
    }
    return null;
  }

  static Size getSizeFromString(String enumAsString) {
    for (Size element in Size.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      }
    }
    return null;
  }

  static LightingType getLightingTypeFromString(String enumAsString) {
    for (LightingType element in LightingType.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      }
    }
    return null;
  }

  static CreatureColor getColorFromString(String enumAsString) {
    for (CreatureColor element in CreatureColor.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      }
    }
    return null;
  }

  static Weather getWeatherFromString(String enumAsString) {
    for (Weather element in Weather.values) {
      if (element.toString().split('.').last == enumAsString) {
        return element;
      }
    }
    return null;
  }

  static Season getSeasonFromString(String enumAsString) {
    for (Season element in Season.values) {
      if (element.toString().split('.').last.toLowerCase() ==
          enumAsString.toLowerCase()) {
        return element;
      }
    }
    return null;
  }
}

class ActiveMonths {
  List<Thern> northern;
  List<Thern> southern;

  ActiveMonths(this.northern, this.southern);

  ActiveMonths.fromJson(Map<String, dynamic> json) {
    if (json['northern'] != null) {
      northern = new List<Thern>();
      json['northern'].forEach((v) {
        northern.add(new Thern.fromJson(v));
      });
    }
    if (json['southern'] != null) {
      southern = new List<Thern>();
      json['southern'].forEach((v) {
        southern.add(new Thern.fromJson(v));
      });
    }
  }
}

class Thern {
  int month;
  bool isAllDay;
  List<List<String>> activeHours;
  Season season;

  Thern(this.month, this.isAllDay, this.season);

  Thern.fromJson(Map<String, dynamic> json) {
    month = json['month'];
    isAllDay = json['isAllDay'];
    if (json['activeHours'] != null) {
      activeHours = List<List<String>>();
      json['activeHours'].forEach((v) {
        activeHours.add(v.cast<String>());
      });
    }
    season = EnumConverter.getSeasonFromString(json['season']);
  }
}

enum SourceSheet { Fish, Insects }

enum Size { The1X1, The2X1, The2X2, The3X2 }

enum LightingType {
  Emission,
  Fluorescent,
}

enum CreatureColor {
  Beige,
  Black,
  Blue,
  Brown,
  Gray,
  Green,
  LightBlue,
  Orange,
  Pink,
  Purple,
  Red,
  White,
  Yellow,
}

enum Weather {
  AnyExceptRain,
  AnyWeather,
  RainOnly,
}

enum Season {
  Autumn,
  Spring,
  Summer,
  Winter,
}

enum ShadowSize { XSmall, Small, Medium, Large, XLarge, XXLarge, Unknown }

enum Where { Sea, River, RiverMouth, RiverTop, Pond, Pier, Unknown }
