class Translation {
  final String id;
  final String ref;
  final Map localization;

  Translation(this.id, this.ref, this.localization);

  Translation.fromJson(Map<String, dynamic> json)
      : this.id = json['id'],
        this.ref = json['ref'],
        this.localization = json['localization'] as Map;
}