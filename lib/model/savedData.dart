import 'package:flutter/material.dart';

class UserData {
  String name;
  String islandName;
  bool north;
  bool sortSpawn;
  bool animatedBackground;

  UserData();

  UserData.fromJson(Map<String, dynamic> jsonString) :
        name = jsonString['name'],
        islandName = jsonString['islandName'],
        north = jsonString['north'],
        sortSpawn = jsonString['sortSpawn'],
        animatedBackground = jsonString['animatedBackground'];


  Map<String, dynamic> toJson() => {
    'name': name,
    'islandName': islandName,
    'north': north,
    'sortSpawn': sortSpawn,
    'animatedBackground': animatedBackground,
  };

  bool getNorth() {
    debugPrint('getNorth: $north');
    if (north == null) {
      return true;
    }
    return north;
  }

  bool getSortSpawn() {
    debugPrint('getSortSpawn: $sortSpawn');
    if (sortSpawn == null) {
      return true;
    }
    return sortSpawn;
  }

  bool getAnimatedBackground() {
    debugPrint('getAnimatedBackground: $animatedBackground');
    if (animatedBackground == null) {
      animatedBackground = true;
    }
    return animatedBackground;
  }
}

class Catches {
  List<DailyCatches> dailyCatches;

  Map<String, bool> catched;

  Catches(this.dailyCatches, this.catched);

  int getAllCatchedFished() {
    int result = 0;
    dailyCatches.forEach((daily) {
      daily.catches.forEach((key, value) {
        result += value;
      });
    });
    return result;
  }

  bool isCatched(String uniqueEntryId) {
    return catched[uniqueEntryId] ?? false;
  }

  List<String> getAllFishUniqueIds() {
    List<String> result = [];
    if (dailyCatches != null && dailyCatches.length > 0) {
      result = dailyCatches[0].catches.keys.map((e) => e.toString()).toList();
    }
    return result;
  }

  int getAllCatchesForFish(String uniqueEntryId) {
    int result = 0;
    dailyCatches
        .forEach((daily) {
      result += daily.catches[uniqueEntryId];
    });
    return result;
  }

}

class DailyCatches {
  String key;
  String formattedDate;
  Map catches;

  DailyCatches(this.key, this.formattedDate, this.catches);

  DailyCatches.fromJson(Map<String, dynamic> jsonString) :
    key = jsonString['key'],
    formattedDate = jsonString['formattedDate'],
    catches = jsonString['catches'] as Map;


  Map<String, dynamic> toJson() => {
    'key': key,
    'formattedDate': formattedDate,
    'catches': catches,
  };
}